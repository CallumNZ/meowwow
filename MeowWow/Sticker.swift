//
//  Sticker.swift
//  MeowWow
//
//  Created by Callum Morris on 2/04/15.
//  Copyright (c) 2015 Callum Morris. All rights reserved.
//

import Foundation
import UIKit

protocol StickerTapped: NSObjectProtocol {
    func stickerTapped(_ sticker: Sticker)
}
                            //added so multiple gestures can be done at one time.
class Sticker: UIImageView, UIGestureRecognizerDelegate {
    
    weak var delegate: StickerTapped?
    var dragStartPositionRelativeToCenter : CGPoint?
    var flipStatus = false
    var lockStatus = false //To stop panning,rotating,pinching when locked. Tapping to select still allowed.
    
    override init(image: UIImage?) {
        super.init(image: image)
        
        self.layer.borderColor = UIColor.purple.cgColor
        self.isUserInteractionEnabled = true
        
        //Add gesture recognisers:
        addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(Sticker.handlePan(_:))))
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(Sticker.handleTap(_:))))
        let dTap = UITapGestureRecognizer(target: self, action: #selector(Sticker.handleDoubleTap))
        dTap.numberOfTapsRequired = 2
        addGestureRecognizer(dTap)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func handlePan(_ recognizer:UIPanGestureRecognizer) {
        if lockStatus == true {
            return
        }
        
        //Select sticker for pinch/rotation once panning started.
        if recognizer.state == UIGestureRecognizerState.began {
            if let delegate = self.delegate {
                delegate.stickerTapped(self)
            }
        }
        
        let translation = recognizer.translation(in: self.superview!)
        recognizer.view!.center = CGPoint(x:recognizer.view!.center.x + translation.x,
            y:recognizer.view!.center.y + translation.y)
        recognizer.setTranslation(CGPoint.zero, in: self.superview!)
    }
    
    @objc func handleTap(_ recognizer:UITapGestureRecognizer){
        if let delegate = self.delegate {
            delegate.stickerTapped(self)
        }
    }
    
    @objc func handleDoubleTap(){
        if self.lockStatus == true {
            return
        }
        
        if self.flipStatus == false {
            self.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.flipStatus = true
        }
        else {
            self.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.flipStatus = false
        }
    }
    
    ////These two are called by photoTapViewController3
    @objc func handlePinch(_ recognizer : UIPinchGestureRecognizer) {
        if self.lockStatus == true {
            return
        }
        
        self.transform = self.transform.scaledBy(x: recognizer.scale, y: recognizer.scale)
        recognizer.scale = 1
    }
    @objc func handleRotate(_ recognizer : UIRotationGestureRecognizer) {
        if self.lockStatus == true {
            return
        }
        if self.flipStatus == true {
            recognizer.rotation = 0 - recognizer.rotation //Accounts for sticker being flipped or not.
        }
        self.transform = self.transform.rotated(by: recognizer.rotation)
        recognizer.rotation = 0
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool { //optional delegate method for UIGestureRecogniserDelegate protocol. Not using really.
        return true
    }
}


    
