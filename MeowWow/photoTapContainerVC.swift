//
//  photoTapContainerVC.swift
//  MeowWow
//
//  Created by Callum Morris on 2/08/15.
//  Copyright (c) 2015 Callum Morris. All rights reserved.
//

import UIKit
import Social //for Facebook and Twitter functionality.

class photoTapContainerVC: UIViewController, categoryTappedDelegate, buttonDelegate, containerDelegate {

    var CIP:IndexPath!
    var myCats: Array<Array<Photo>>?
    weak var delegate: updateDelegate? //delegates method to ContainerViewController. Protocol declared in addPhotoVC.
    var editMode: Bool = false
    
    enum ContainerViewTags: Int {
        case containerCV = 1,containerTable,containerBar,containerHeader,containerDelete
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //  Create the three views used in the container view.
        let photoTapViewVC :photoTapViewController3 = photoTapViewController3(collectionViewLayout: UICollectionViewFlowLayout(), currentIndexPath: CIP) 
        let tableVC:photoTapTableVC = photoTapTableVC()
        let barVC:buttonBarVC = buttonBarVC()
        photoTapViewVC.collectionView!.tag = ContainerViewTags.containerCV.rawValue
        tableVC.view.tag = ContainerViewTags.containerTable.rawValue
        barVC.view.tag = ContainerViewTags.containerBar.rawValue
        
        //Make header
        let header = makeHeader()
        header.tag = ContainerViewTags.containerHeader.rawValue
        self.view.addSubview(header)
        
        // Position/display the three views on the container view :)
        self.addChildViewController(photoTapViewVC)
        self.addChildViewController(barVC)
        self.addChildViewController(tableVC)
        self.view.addSubview(photoTapViewVC.collectionView!)
        self.view.addSubview(barVC.view)
        self.view.addSubview(tableVC.view)
        photoTapViewVC.didMove(toParentViewController: self)
        barVC.didMove(toParentViewController: self)
        tableVC.didMove(toParentViewController: self)
        
        //Make views invisible before animations take place. Helps with conditions where views are displayed during ifLayoutNeeded before animation. Made visible in UIAnimations.swift
        photoTapViewVC.collectionView!.isHidden = true
        
        //Make iPhoneX UIView to colour status bar (if needed)
        if UIScreen.main.bounds.size.height == 812 {
            self.view.addSubview(makeiPhoneXView())
        }
        
        //Create initial constraints. Required at this point so that the image animates to the correct position during the transition.
        self.createConstraints()
        self.view.layoutIfNeeded()
        
        //Set delegates
        tableVC.delegate = self
        barVC.delegate = self //For the buttons.
        
        //Add buttons to header, basing frame on how big header made with constraints.
        let headerW = UIScreen.main.bounds.size.width
        let button = makeBackButton(controller: self)
        let button2 = makeDeletePhotoButton()
        button2.frame = CGRect(x: headerW-100, y: 0, width: 100, height: 40)
        header.addSubview(button) //Buttons added to header view to avoid having to constrain it with AutoLayout.
        header.addSubview(button2)
        
        //Tag used for changing status of delete button on header (see function below).
        button2.tag = ContainerViewTags.containerDelete.rawValue
        
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    ////CONVENIENCE METHODS////
    func returnPhotoFrame() -> CGRect {
        let frame = self.view.viewWithTag(ContainerViewTags.containerCV.rawValue)?.frame
        return frame!
    }
    func returnPhotoLinearIndex() -> IndexPath {
        let collectionView = returnUICollectionView()
        let indexPath = collectionView.indexPathsForVisibleItems[0] //Gets array of visible cell indexPaths, whose count is always 1, so current is [0]
        return indexPath
    }
    func returnCurrentPhoto() -> UIImage {
        let collectionView = returnUICollectionView()
        let indexPath = collectionView.indexPathsForVisibleItems[0] //Gets array of visible cell indexPaths, whose count is always 1, so current is [0]
        let cell = collectionView.cellForItem(at: indexPath) as! photoTapViewCell2
        let photo = cell.photoFrame.image!
        return photo
    }
    func returnUICollectionView() -> UICollectionView {
        let collectionView = self.view.viewWithTag(ContainerViewTags.containerCV.rawValue) as! UICollectionView
        return collectionView
    }
    func hidePhoto(hide: Bool) {
        if hide == true {
            self.view.viewWithTag(ContainerViewTags.containerCV.rawValue)?.isHidden = true
        } else {
            self.view.viewWithTag(ContainerViewTags.containerCV.rawValue)?.isHidden = false
        }
    }
    
    //////////////////DELEGATED METHODS://///////////////////////
    
    func controller(_ controller: photoTapTableVC, category: Int) {
        
        //Getting frame of table view, then the collectionView matches it. This avoids having to muck around with AutoLayout for this part.
        let sameframe: CGRect = controller.view.frame
        let collectionVC:photoTapCollectionVC = photoTapCollectionVC(collectionViewLayout: UICollectionViewFlowLayout(), currentIndexPath: CIP, frame: sameframe, category: category)
        
        //Set delegate
        collectionVC.delegate = self.childViewControllers.first as! photoTapViewController3
        collectionVC.delegate2 = self
        collectionVC.view.translatesAutoresizingMaskIntoConstraints = false
        
        //Adding the child. Note it's important for transition calls to be after didMove call for child's viewWillAppear etc to run.
        self.addChildViewController(collectionVC)
        self.view.addSubview(collectionVC.collectionView!)
        collectionVC.didMove(toParentViewController: self)
        collectionVC.beginAppearanceTransition(true, animated: false)
        collectionVC.endAppearanceTransition()
        
        //Make dismiss button on buttonBarVC interactable,purple.
        let bar = self.childViewControllers[1] as! buttonBarVC //gets second child which is the bar.
        let dB: UIButton = bar.buttonList[0]
        dB.isUserInteractionEnabled = true
        dB.isEnabled = true
    }
    
    //Called when dismiss button on photoTapCollection used.
    func dismissSelected(_ controller: buttonBarVC) {

        let count = self.childViewControllers.count
        let collection = self.childViewControllers[count-1] as? photoTapCollectionVC
        
        if collection != nil {
            //Child removed in the completion block of the animation.
            collection!.beginAppearanceTransition(false, animated: true)
            collection!.endAppearanceTransition()
            
            //Make dismiss button on buttonBarVC uninteractable, greyed out.
            let dB: UIButton = controller.buttonList[0]
            dB.isUserInteractionEnabled = false
            dB.isEnabled = false
        }
    }
    
    func makeDeleteSaveActive(_ binary: Bool) { //Changes status of undo (cancel), save, and lock buttons on the buttonBarVC.
        //Get button bar child.
        let bar = self.childViewControllers[1] as! buttonBarVC
        let dB: UIButton = bar.buttonList[1]
        let sB: UIButton = bar.buttonList[2]
        let lB: UIButton = bar.buttonList[3]
        
        if binary == true {
            //Make delete all button on buttonBarVC interactable.
            dB.isUserInteractionEnabled = true
            dB.isEnabled = true
            //Make save button on buttonBarVC interactable.
            sB.isUserInteractionEnabled = true
            sB.isEnabled = true
            //Make lock button on buttonBarVC interactable.
            lB.isUserInteractionEnabled = true
            lB.isEnabled = true
        }
        else{
            //Make delete all button on buttonBarVC uninteractable, greyed out.
            dB.isUserInteractionEnabled = false
            dB.isEnabled = false
            //Make save button on buttonBarVC uninteractable, greyed out.
            sB.isUserInteractionEnabled = false
            sB.isEnabled = false
            //Make lock button on buttonBarVC uninteractable, greyed out.
            lB.isUserInteractionEnabled = false
            lB.isEnabled = false
        }
    }
    
    func makeDeleteButtonActive(_ binary: Bool) {
        let header = self.view.viewWithTag(ContainerViewTags.containerHeader.rawValue)
        let deleteButton = header!.viewWithTag(ContainerViewTags.containerDelete.rawValue) as! UIButton
        
        if binary == true {
            //Make delete button on header interactable.
            deleteButton.setTitleColor(UIColor.white, for: UIControlState())
            deleteButton.isUserInteractionEnabled = true
            deleteButton.isEnabled = true
            self.editMode = false
        }
        else{
            //Make delete all button on header uninteractable, greyed out.
            let greyedOut = UIColor(red: 221/255, green: 160/255, blue: 221/255, alpha: 0.5)
            deleteButton.setTitleColor(greyedOut, for: UIControlState())
            deleteButton.isUserInteractionEnabled = false
            deleteButton.isEnabled = false //For changing image of delete button to greyed out.
            self.editMode = true
        }
    }
    
    func cancelSelected(_ controller: buttonBarVC) {
        //Get photoTapViewController child.
        let pTVC = self.childViewControllers[0] as! photoTapViewController3
        if pTVC.removeAllStickers() == 0 {
            self.makeDeleteSaveActive(false)
            self.makeDeleteButtonActive(true)
        }
    }
    
    func lockSelected(_ controller: buttonBarVC) {
        //Get photoTapViewController child.
        let pTVC = self.childViewControllers[0] as! photoTapViewController3
        for sticker in pTVC.stickerList {
            sticker.lockStatus = true
        }
    }
    
    func saveSelected(_ controller: buttonBarVC) {
        //iOS 11 has deprecated the ability to login to Facebook/Twitter accounts via system settings. This has rendered SLComposeViewController useless. I have decided that just allowing saving to device on iOS 11 and then sharing via a social media app is a minor inconvenience, and the sharing capability of this app does not justify the time/overhead involved in integrating the relevant third party SDKs.
        
        let alert = UIAlertController(title: "Mewow!", message: "", preferredStyle: UIAlertControllerStyle.alert)
        let facebookAction = UIAlertAction(title: "Share to Facebook", style: .default) {(action) in
            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook){
                let facebookSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
                facebookSheet.setInitialText("")
                facebookSheet.add(self.screenshot())
                self.present(facebookSheet, animated: true, completion: nil)
            } else {
                let alert = UIAlertController(title: "Accounts", message: "Please login to a Facebook account in Settings to share.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        let twitterAction = UIAlertAction(title: "Post on Twitter", style: .default) {(action) in
            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter){
                let twitterSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
                twitterSheet.setInitialText("")
                twitterSheet.add(self.screenshot())
                self.present(twitterSheet, animated: true, completion: nil)
            } else { 
                let alert = UIAlertController(title: "Accounts", message: "Please login to a Twitter account in Settings to share.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        let saveAction = UIAlertAction(title: "Save to Device", style: .default) {(action) in
            UIImageWriteToSavedPhotosAlbum(self.screenshot(), self, #selector(photoTapContainerVC.image(_:didFinishSavingWithError:contextInfo:)), nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {(action) in
            return
        }
        
        if #available(iOS 11.0, *) {
            alert.addAction(saveAction)
            alert.addAction(cancelAction)
        } else {
            alert.addAction(facebookAction)
            alert.addAction(twitterAction)
            alert.addAction(saveAction)
            alert.addAction(cancelAction)
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer) {
        if error == nil {
            let ac = UIAlertController(title: "Saved!", message: "Purr :3", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(ac, animated: true, completion: nil)
        } else {
            let ac = UIAlertController(title: "Save error", message: error?.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(ac, animated: true, completion: nil)
        }
    }
    
    /////////////////////////////////////////////////////////////////////////////
    
    func screenshot() -> UIImage {
        //Get y axis position
        var yAxis: CGFloat = 41 //Accounts for header height
        if UIScreen.main.bounds.size.height == 812 {
            yAxis += 40 //Accounts for iPhoneX notch height
        }
        
        //Get position of photo
        let pTVC = self.childViewControllers[0] as! photoTapViewController3
        let pTVCwidth = pTVC.collectionView!.bounds.width
        let frameSize = CGSize(width: pTVCwidth, height: pTVCwidth)
        let photoBounds = CGRect(x: 0, y: -yAxis, width: self.view.bounds.width, height: self.view.bounds.height+2)
        
        //Get rid of border just in case.
        pTVC.tappedSticker!.layer.borderWidth = 0
  
        //Takes screenshot of picture + stickers.
        UIGraphicsBeginImageContextWithOptions(frameSize, true, 0.0) //0.0 makes it resolution of device's screen.
        self.view.drawHierarchy(in: photoBounds, afterScreenUpdates: true) //made this false to stop scale glitch.
        pTVC.collectionView!.layer.render(in: UIGraphicsGetCurrentContext()!)
        let screenshot = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return screenshot!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func backButtonTapped(_ sender: UIButton!) {
        if self.editMode == false {
            //self.dismiss(animated: false, completion: {self.delegate?.updateMCVC()}) //Using this closure solved the problem where the RAM wasn't being deallocated once photoTapViewController was dismissed.
            let buttonBar = self.childViewControllers[1] as! buttonBarVC
            dismissSelected(buttonBar)
            self.dismiss(animated: true, completion: nil)

        } else {
            let alert = UIAlertController(title: "Mmrow?", message: "Going back will lose the stickers you've put on.", preferredStyle: UIAlertControllerStyle.alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) {(action) in
                self.dismiss(animated: true, completion: nil) //used to use self.view.'subview.removeFromSuperView()
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .default) {(action) in
                return
            }
            alert.addAction(OKAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func deletePhotoButtonTapped(_ sender: UIButton!) {
        
        let alert = UIAlertController(title: "Mmrow?", message: "Are you sure you want to delete this photo?", preferredStyle: UIAlertControllerStyle.alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default) {(action) in
            let pTVC = self.childViewControllers[0] as! photoTapViewController3
            let cellInViewIndex = pTVC.returnCellinViewIndex()
            
            let pL = PhotoLibrary()
            let cellInView = pL.photoForIndexPath(cellInViewIndex, catsArray: self.myCats!)
            pL.deletePhoto(photoDate: cellInView.photoDate, catName: cellInView.catName)
            
            pTVC.dismiss(animated: true, completion: {self.delegate?.updateMCVC()})
        }
        
        let noAction = UIAlertAction(title: "No", style: .default) {(action) in
            return
        }
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    //These two methods lock device in portrait orientation. Need to put into each view controller (the containers) that needs to be locked.
    override var shouldAutorotate : Bool {
        return false
    }
    
    override var prefersStatusBarHidden : Bool {
        //For iPhone X
        if UIScreen.main.bounds.size.height == 812 {
            return false
        }
        return true
    }

    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        //return UIInterfaceOrientation.Portrait.rawValue
        //(Internet answer): So I wasn't aware but in Swift 2.0 multiple bitmasks are now put in an array:
        return [UIInterfaceOrientationMask.portrait, UIInterfaceOrientationMask.portraitUpsideDown]
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
