//
//  photoTapViewController3.swift
//  MeowWow
//
//  Created by Callum Morris on 26/03/15.
//  Copyright (c) 2015 Callum Morris. All rights reserved.
//

import UIKit
import Foundation
import CoreData

let photoTapViewCellIdentify = "photoTapViewCellIdentify"

class photoTapViewController3: UICollectionViewController, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate, stickerDelegate, StickerTapped, NSFetchedResultsControllerDelegate {
    
    var pullOffset = CGPoint.zero
    var stickerList: Array<Sticker> = Array<Sticker>()
    var tappedSticker: Sticker?
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!

    init(collectionViewLayout theLayout: UICollectionViewFlowLayout!, currentIndexPath indexPath: IndexPath){
        
        //Sets up the CollectionView and ViewFlowLayout
        super.init(collectionViewLayout: theLayout)
        
        let cView :UICollectionView = self.collectionView!;
        let screen = self.view.frame.width
        cView.frame = CGRect(x: 0, y: 0, width: screen, height: screen)
        cView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        cView.isPagingEnabled = true
        cView.register(photoTapViewCell2.self, forCellWithReuseIdentifier: photoTapViewCellIdentify)
        
        theLayout.itemSize = CGSize(width: screen, height: screen)
        theLayout.scrollDirection = UICollectionViewScrollDirection.horizontal
        theLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        theLayout.minimumInteritemSpacing = 0
        theLayout.minimumLineSpacing = 0
        
        cView.performBatchUpdates({
            cView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
            }, completion: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.fetchedResultsController.managedObjectContext.reset()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initializeFetchedResultsController()

        // Do any additional setup after loading the view.
    }
    
    func initializeFetchedResultsController() {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "PhotoCD")
        let catNameSort = NSSortDescriptor(key: "cat.catName", ascending: true)
        let dateSort = NSSortDescriptor(key: "dateCreated", ascending: true)
        request.sortDescriptors = [catNameSort, dateSort]
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: managedContext, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = self
        
        do {
            try fetchedResultsController.performFetch()
        } catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        guard let cell: photoTapViewCell2 = collectionView.dequeueReusableCell(withReuseIdentifier: photoTapViewCellIdentify, for: indexPath) as? photoTapViewCell2 else {
            fatalError("Wrong cell type dequeued")
        }
        guard let object = self.fetchedResultsController?.object(at: indexPath) as? PhotoCD else {
            fatalError("Attempt to configure cell without a managed object")
        }
        cell.photoFrame.image = UIImage(data: (object.value(forKey: "photoData") as? Data)!)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        guard let sections = fetchedResultsController.sections else {
            fatalError("No sections in fetchedResultsController")
        }
        let sectionInfo = sections[section]
        return sectionInfo.numberOfObjects
    }
    
    //Adding/Deleting sticker functions
    func addListStickers(){
        for s in self.stickerList{
            self.collectionView!.addSubview(s)
        }
    }
    func removeAllStickers() -> Int{ //changed to remove tapped sticker. Returns zero if last sticker removed.
        var index = 0
        for sticker in self.stickerList {
            if sticker == self.tappedSticker! {
                sticker.removeFromSuperview()
                self.stickerList.remove(at: index)
                continue
            }
            index += 1
        }
        
        if self.stickerList.count == 0 {
            self.collectionView!.isScrollEnabled = true //Unlock scrolling since no more stickers on screen.
            return 0
        }
        return 1
    }
    
    func returnCellinViewIndex() -> IndexPath{
        //This finds what photo is being displayed, returns that cell's linear indexPath.
        let photoInViewArray: Array = self.collectionView!.indexPathsForVisibleItems
        let photoInViewIndex: IndexPath = photoInViewArray.first as IndexPath!
        return photoInViewIndex
    }
    
    ///////DELEGATE METHODS///////////////////////////////////////////////////////////////////////////////////
    
    func stickerSelected(_ controller: photoTapCollectionVC, cellview: UIView, sticker: UIImage) {
        
        //You pass the sticker image selected to create Sticker.
        let selectedSticker: Sticker = Sticker(image: sticker)
        selectedSticker.delegate = self
        
        //This finds what photo is being displayed so that the sticker is put in the right place.
        let cellInViewIndex = self.returnCellinViewIndex()
        let cellInView: photoTapViewCell2 = self.collectionView!.cellForItem(at: cellInViewIndex) as! photoTapViewCell2
        
        //Getting frame locations/sizes based on photo in view.
        let pX = cellInView.frame.origin.x
        let pY = cellInView.frame.origin.y
        let pW = cellInView.frame.width
        let pH = cellInView.frame.height
        
        let stickerFrame: CGRect = CGRect(x: pX, y: pY, width: pW/2, height: pH/2)
        selectedSticker.frame = stickerFrame
        selectedSticker.contentMode = UIViewContentMode.scaleAspectFit //not squished.
        
        //Add sticker to list and then call function to add list stickers as subviews.
        self.stickerList.append(selectedSticker)
        self.addListStickers()
        
        //Make sticker the current 'tapped' sticker
        self.stickerTapped(selectedSticker)
        
        //Lock scrolling, add tap recogniser to unselect (remove border) from tappedSticker when outside of any stickers tapped.
        self.collectionView!.isScrollEnabled = false
        let tapObserver = UITapGestureRecognizer(target: self, action: #selector(photoTapViewController3.handleTapOut))
        self.collectionView!.addGestureRecognizer(tapObserver)
    }
    
    @objc func handleTapOut() {
        self.tappedSticker!.layer.borderWidth = 0
    }
    
    func stickerTapped(_ sticker: Sticker) {
        //Remove outline from previously selected sticker
        if self.tappedSticker != nil {
            self.tappedSticker!.layer.borderWidth = 0
        }
        
        self.collectionView!.gestureRecognizers?.forEach(sticker.removeGestureRecognizer) //remove all gestureRecognisers from collectionView.
        self.tappedSticker = sticker
        self.collectionView!.bringSubview(toFront: self.tappedSticker!) //brings to front.
        
        //Add gesture recognizers for pinch and rotate. These are in here instead of in the sticker so that anywhere on photo registers them.
        let pinch = UIPinchGestureRecognizer(target: self.tappedSticker!, action: #selector(self.tappedSticker!.handlePinch(_:)))
        let rotate = UIRotationGestureRecognizer(target: self.tappedSticker!, action: #selector(self.tappedSticker!.handleRotate(_:)))
        self.collectionView!.addGestureRecognizer(pinch)
        self.collectionView!.addGestureRecognizer(rotate)
        
        //Outline around selected sticker.
        self.tappedSticker!.layer.borderWidth = 2
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool { //optional delegate method for UIGestureRecogniserDelegate protocol. Not using currently.
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //These two methods (added 23/3/15) lock device in portrait orientation. Need to put into each view controller that needs to be locked.
    override var shouldAutorotate : Bool {
        return false
    }
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        //return UIInterfaceOrientation.Portrait.rawValue
        //(Internet answer): So I wasn't aware but in Swift 2.0 multiple bitmasks are now put in an array:
        return [UIInterfaceOrientationMask.portrait, UIInterfaceOrientationMask.portraitUpsideDown]
    }
    
    //Added so that when collectionview cell tapped, all of the embedded tableviewcells aren't highlighted.
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
