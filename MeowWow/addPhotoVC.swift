//
//  addPhotoVC.swift
//  MeowWow
//
//  Created by Callum Morris on 1/02/16.
//  Copyright © 2016 Callum Morris. All rights reserved.
//

import UIKit

protocol updateDelegate: NSObjectProtocol {
    func updateMCVC()
}

class addPhotoVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    weak var delegate: updateDelegate?
    
    let scrollView = UIScrollView(frame: UIScreen.main.bounds)
    let contentView = UIView()
    let picker = UIImagePickerController() //For choosing a photo.
    var wheelData = Array<String>()
    let frame = UIImageView()
    var takePhoto: Bool? //If true, launch camera instead of library.
    var trigger: Bool = true //For displaying image picker or dismissing addPhotoVC
    
    enum AddPhotoViewTags: Int {
        case addPhotoHeader = 1,addPhotoFrame,addButton,newButton,addPhotoLabel,addPhotoScroll,addPhotoTextBox
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self  //Wiring up the delegate for choosing/taking a photo.
        
        self.view = self.scrollView
        self.view.backgroundColor = UIColor.white
        self.contentView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.scrollView.addSubview(contentView)
        //All the subviews are added to contentView, so that they're all in one child view of the scrollView. This is needed for AutoLayout to work properly.
        
        //Make back,add button/header/image/label view
        let header = makeHeader()
        let button = makeBackButton(controller: self)
        let addBut = makeAddButton()
        let newBut = makeNewCatButton()
        let lb1 = makeCatNameLabel() //"Cat Name:"
        header.tag = AddPhotoViewTags.addPhotoHeader.rawValue
        self.frame.tag = AddPhotoViewTags.addPhotoFrame.rawValue
        addBut.tag = AddPhotoViewTags.addButton.rawValue
        newBut.tag = AddPhotoViewTags.newButton.rawValue
        lb1.tag = AddPhotoViewTags.addPhotoLabel.rawValue
        self.contentView.addSubview(self.frame)
        self.contentView.addSubview(header)
        self.contentView.addSubview(lb1)
        self.contentView.addSubview(addBut)
        self.contentView.addSubview(newBut)
        header.addSubview(button)
        
        //Make iPhoneX UIView to colour status bar (if needed)
        if UIScreen.main.bounds.size.height == 812 {
            let view = makeiPhoneXView()
            self.contentView.addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        //Make picker wheel for choosing cat name
        let scrollWheel = UIPickerView()
        scrollWheel.dataSource = self
        scrollWheel.delegate = self
        scrollWheel.backgroundColor = UIColor.clear
        scrollWheel.selectRow(0, inComponent: 0, animated: false)
        scrollWheel.tag = AddPhotoViewTags.addPhotoScroll.rawValue
        self.pickerView(scrollWheel, didSelectRow: 0, inComponent: 0) //Selects first row in picker.
        self.contentView.addSubview(scrollWheel)
        
        //Frame setup
        self.frame.contentMode = UIViewContentMode.scaleAspectFit
        self.createConstraints(header, pho: frame, new: newBut, pick: scrollWheel, lab1: lb1, add:addBut)
        
        //Observes when keyboard appears/disappears
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //Looks for single or multiple taps. This is for dismissing the keyboard with a tap outside the keyboard.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(addPhotoVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        // Do any additional setup after loading the view.
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @objc private func keyboardWillShow(notification: Notification) {
        //Gets height of keyboard, offsets scrollview's content by this much, then scrolls automatically so that the text field is visible (in line with 'Cat Name' label (tag 5))
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        
        let catNameLabel = self.contentView.viewWithTag(AddPhotoViewTags.addPhotoLabel.rawValue)!
        let labelOrigin = catNameLabel.frame.origin
        let labelHeight = catNameLabel.frame.height
        var visibleRect:CGRect = self.contentView.frame
        visibleRect.size.height -= keyboardHeight //Size of view not obscured by keyboard.        
   
        if !visibleRect.contains(labelOrigin){ //If label frame not in this visible area, scrollview ContentOffset changed.
            let scrollPoint: CGPoint = CGPoint(x: 0.0, y: labelOrigin.y - visibleRect.height + labelHeight)
            self.scrollView.setContentOffset(scrollPoint, animated: true)
        }
    }
    
    @objc private func keyboardWillHide(notification: Notification) { //Called when keyboard dismissed.
        self.scrollView.setContentOffset(CGPoint.zero, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) { //used to be viewDidAppear, didn't make much difference to transition speed.
        
        if self.takePhoto == false && self.trigger == true && self.frame.image == nil{
            self.trigger = false
            self.photoFromLibrary()
        }
        else if self.takePhoto == true && self.trigger == true && self.frame.image == nil {
            self.trigger = false
            self.photoFromCamera()
        }
        else if self.frame.image == nil {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    deinit { //Remove keyboard observer when self is deinitialised (I'm guessing).
        NotificationCenter.default.removeObserver(self);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /////////Delegate methods://////////////////////////////////////////////////////////////////////////////////////////////
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pic = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.frame.image = pic
        } else {
            print("Something went wrong when selecting a photo")
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: self.delegate?.updateMCVC)
    }
    
    //Scroll wheel delegate methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return wheelData.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return wheelData[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    func photoFromLibrary(){
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        present(picker, animated: true, completion: nil)
    }
    
    func photoFromCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.sourceType = .camera;
            picker.allowsEditing = false
            self.present(picker, animated: true, completion: nil)
        }
    }
    
    @objc func backButtonTapped(_ sender: UIButton!) {
        self.dismiss(animated: true, completion: self.delegate?.updateMCVC)
    }
    
    @objc func addButtonTapped(_ sender: UIButton!) { //Saves photo to Core Data using instance of PhotoLibrary.
        var catName: String?
        if (self.contentView.viewWithTag(AddPhotoViewTags.addPhotoTextBox.rawValue) != nil) { //If UITextFeld is there (new name entered), use this name.
            let textField = self.contentView.viewWithTag(AddPhotoViewTags.addPhotoTextBox.rawValue)! as! UITextField
            catName = textField.text
        }
        else {
            let picker = self.contentView.viewWithTag(AddPhotoViewTags.addPhotoScroll.rawValue) as! UIPickerView
            catName = self.wheelData[picker.selectedRow(inComponent: 0)]
        }

        if catName == "" { //Checks to see whether cat name has been chosen.
            let alert = UIAlertController(title: "Woops!", message: "Choose your cat's name first.", preferredStyle: UIAlertControllerStyle.alert)
            let buttonAction = UIAlertAction(title: "OK", style: .default) {(action) in
            }
            alert.addAction(buttonAction)
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        let photo = self.frame.image!
        let photoDate = NSDate()
        let x = PhotoLibrary()
        x.savePhoto(catName!, photo: photo, photoDate: photoDate)
        
        self.delegate!.updateMCVC()
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func newCatButtonTapped(_ sender: UIButton!) {

        //If new Cat button has already been tapped before.
        var textBox = UITextField()
        if (self.contentView.viewWithTag(AddPhotoViewTags.addPhotoTextBox.rawValue) != nil) {
            textBox = self.contentView.viewWithTag(AddPhotoViewTags.addPhotoTextBox.rawValue) as! UITextField
            textBox.becomeFirstResponder()
        }
        else {
            textBox = UITextField()
            textBox.placeholder = ""
            textBox.delegate = self
            textBox.autocorrectionType = UITextAutocorrectionType.no
            textBox.tag = AddPhotoViewTags.addPhotoTextBox.rawValue
            self.contentView.addSubview(textBox)
            
            //Remove previous constraints
            self.contentView.removeConstraints(self.contentView.constraints)
            
            //Remove picker view, create new constraints, then update constraints.
            self.contentView.viewWithTag(AddPhotoViewTags.addPhotoScroll.rawValue)?.removeFromSuperview()
            let header = self.contentView.viewWithTag(AddPhotoViewTags.addPhotoHeader.rawValue)!
            let photo = self.contentView.viewWithTag(AddPhotoViewTags.addPhotoFrame.rawValue) as! UIImageView
            let but1 = self.contentView.viewWithTag(AddPhotoViewTags.addButton.rawValue) as! UIButton
            let but2 = self.contentView.viewWithTag(AddPhotoViewTags.newButton.rawValue) as! UIButton
            let label = self.contentView.viewWithTag(AddPhotoViewTags.addPhotoLabel.rawValue) as! UILabel
            let textField = self.contentView.viewWithTag(AddPhotoViewTags.addPhotoTextBox.rawValue) as! UITextField
            self.createNewConstraints(header, pho: photo, new: but2, add: but1, lab1: label, text: textField)
            self.updateViewConstraints()

            textBox.becomeFirstResponder() //Makes keyboard show.
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.dismissKeyboard()
        return false
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
}


