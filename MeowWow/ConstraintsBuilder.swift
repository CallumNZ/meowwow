//
//  ConstraintsBuilder.swift
//  MeowWow
//
//  Created by CallumMorris on 12/12/17.
//  Copyright © 2017 Callum Morris. All rights reserved.
//

//Contains several extensions to the classes that require significant constraint building.

import Foundation
import UIKit

extension photoTapContainerVC {
    
    func createConstraints() {
    
        //Gets container views:
        let col = self.view.viewWithTag(ContainerViewTags.containerCV.rawValue)!
        let tab = self.view.viewWithTag(ContainerViewTags.containerTable.rawValue)!
        let bar = self.view.viewWithTag(ContainerViewTags.containerBar.rawValue)!
        let head = self.view.viewWithTag(ContainerViewTags.containerHeader.rawValue)!
        
        //Turns off translatesAutoresizingMaskIntoConstraints
        col.translatesAutoresizingMaskIntoConstraints = false
        bar.translatesAutoresizingMaskIntoConstraints = false
        tab.translatesAutoresizingMaskIntoConstraints = false
        head.translatesAutoresizingMaskIntoConstraints = false
        
        //Distance between header and top of screen based on whether it's running on an iPhone X or not.
        var x: Int = 0
        if UIScreen.main.bounds.size.height == 812 {
            x = 40
        }
        //Views to add constraints to:
        let views = Dictionary(dictionaryLiteral: ("c",col),("t",tab),("h",head),("b",bar))
        //Horizontal constraints
        let horizontalConstraintsC = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[c]-0-|", options: [], metrics: nil, views: views)
        let horizontalConstraintsT = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[t]-0-|", options: [], metrics: nil, views: views)
        let horizontalConstraintsH = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[h]-0-|", options: [], metrics: nil, views: views)
        let horizontalContraintsB = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[b]-0-|",options: [], metrics: nil, views: views)
        
        //Device width calculator
        let w = UIScreen.main.bounds.size.width
        
        //Vertical constraints
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(x)-[h(==40)]-0-[c(\(w))]-0-[b(==30)]-0-[t(<=400@20)]|", options: [], metrics: nil, views: views)
        
        self.view.removeConstraints(self.view.constraints)
        self.view.addConstraints(horizontalConstraintsC)
        self.view.addConstraints(horizontalConstraintsT)
        self.view.addConstraints(horizontalConstraintsH)
        self.view.addConstraints(horizontalContraintsB)
        self.view.addConstraints(verticalConstraints)
    }
    
    func createConstraintsForTransition() {
        //Gets container views:
        let col = self.view.viewWithTag(ContainerViewTags.containerCV.rawValue)!
        let tab = self.view.viewWithTag(ContainerViewTags.containerTable.rawValue)!
        let bar = self.view.viewWithTag(ContainerViewTags.containerBar.rawValue)!
        let head = self.view.viewWithTag(ContainerViewTags.containerHeader.rawValue)!
        
        //Turns off translatesAutoresizingMaskIntoConstraints
        col.translatesAutoresizingMaskIntoConstraints = false
        bar.translatesAutoresizingMaskIntoConstraints = false
        tab.translatesAutoresizingMaskIntoConstraints = false
        head.translatesAutoresizingMaskIntoConstraints = false
        
        //Distance between header and top of screen based on whether it's running on an iPhone X or not.
        var x: Int = 0
        if UIScreen.main.bounds.size.height == 812 {
            x = 40
        }
        
        //Views to add constraints to:
        let views = Dictionary(dictionaryLiteral: ("c",col),("t",tab),("h",head),("b",bar))
        //Horizontal constraints
        let horizontalConstraintsC = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[c]-0-|", options: [], metrics: nil, views: views)
        let horizontalConstraintsT = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[t]-0-|", options: [], metrics: nil, views: views)
        let horizontalConstraintsH = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[h]-0-|", options: [], metrics: nil, views: views)
        let horizontalContraintsB = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[b]-0-|",options: [], metrics: nil, views: views)
        
        //Device width calculator
        let w = UIScreen.main.bounds.size.width
        
        //Height of table and bar
        let z = tab.frame.height + bar.frame.height
        
        //Vertical constraints (negative number to move bar and table offscreen)
        //let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(x)-[h(==40)]-0-[c(\(w))]-\(z)-[b(==30)]-0-[t(<=400@20)]-(-\(z))-|", options: [], metrics: nil, views: views)
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(x)-[h(==40)]-0-[c(\(w))]-0-[b(==30)]-0-[t(<=400@20)]-0-|", options: [], metrics: nil, views: views)
        
        //Remove current constraints, then add the new dismisal constraints
        self.view.removeConstraints(self.view.constraints)
        self.view.addConstraints(horizontalConstraintsC)
        self.view.addConstraints(horizontalConstraintsT)
        self.view.addConstraints(horizontalConstraintsH)
        self.view.addConstraints(horizontalContraintsB)
        self.view.addConstraints(verticalConstraints)
        
    }
}

extension addPhotoVC {
    
    func createConstraints(_ head: UIView, pho: UIImageView, new: UIButton, pick: UIPickerView, lab1: UILabel, add: UIButton) -> Void
    {
        //Distance between header and top of screen based on whether it's running on an iPhone X or not.
        var x: CGFloat  = 0
        if UIScreen.main.bounds.size.height == 812 {
            x = 40
        }
        //Views to add constraints to
        let views = Dictionary(dictionaryLiteral: ("h",head),("i",pho),("b1",new),("p",pick),("l1",lab1),("b2",add))
        //Horizontal constraints
        let horizontalConstraintsH = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[h]-0-|", options: [], metrics: nil, views: views)
        let horizontalConstraintsI = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[i]-0-|", options: [], metrics: nil, views: views)
        let horizontalConstraintsP = NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[l1(==90)]-0-[p]-95-|", options: [], metrics: nil, views: views)
        let horizontalConstraintsB = NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[b1]-5-[p]-5-[b2]-5-|", options: [], metrics: nil, views: views)
        
        self.contentView.addConstraints(horizontalConstraintsH)
        self.contentView.addConstraints(horizontalConstraintsP)
        self.contentView.addConstraints(horizontalConstraintsI)
        self.contentView.addConstraints(horizontalConstraintsB)
        
        let w = UIScreen.main.bounds.size.width
        
        //Calculate spacing between 'Cat Name' label and the New Cat and Add buttons.
        let ls = (UIScreen.main.bounds.size.height-x-40-UIScreen.main.bounds.size.width-20)/2 //Label space (Label is 20 high).
        let lbs = (ls-40)/2 //Label button space. 40 is the height of the button.
        
        //Vertical constraints for choosing existing name with picker wheel.
        let verticalConstraints1 = NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(x)-[h(==40)]-0-[i(\(w))]-0-[p]-0-|", options: [], metrics: nil, views: views)
        let verticalConstraints2 = NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(x)-[h(==40)]-0-[i(\(w))]-\(ls)-[l1(==20)]-\(lbs)-[b1(==40)]-\(lbs)-|", options: [], metrics: nil, views: views)
        let verticalConstraints3 = NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(x)-[h(==40)]-0-[i(\(w))]-\(ls)-[l1(==20)]-\(lbs)-[b2(==40)]-\(lbs)-|", options: [], metrics: nil, views: views)
        
        for (_,view) in views {
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        self.contentView.addConstraints(verticalConstraints1)
        self.contentView.addConstraints(verticalConstraints2)
        self.contentView.addConstraints(verticalConstraints3)
    }
    
    func createNewConstraints(_ head: UIView, pho: UIImageView, new: UIButton, add: UIButton, lab1: UILabel, text: UITextField) -> Void
    {
        //Distance between header and top of screen based on whether it's running on an iPhone X or not.
        var x: CGFloat = 0
        if UIScreen.main.bounds.size.height == 812 {
            x = 40
        }
        //Views to add constraints to
        let views = Dictionary(dictionaryLiteral: ("h",head),("i",pho),("b1",new),("b2",add),("l1",lab1),("t",text))
        //Horizontal constraints
        let horizontalConstraintsH = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[h]-0-|", options: [], metrics: nil, views: views)
        let horizontalConstraintsI = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[i]-0-|", options: [], metrics: nil, views: views)
        let horizontalConstraintsP = NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[l1(==90)]-5-[t]-95-|", options: [], metrics: nil, views: views)
        let horizontalConstraintsB = NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[b1]-5-[t]-5-[b2]-5-|", options: [], metrics: nil, views: views)
        
        self.contentView.addConstraints(horizontalConstraintsH)
        self.contentView.addConstraints(horizontalConstraintsP)
        self.contentView.addConstraints(horizontalConstraintsI)
        self.contentView.addConstraints(horizontalConstraintsB)
        
        //Calculate spacing between 'Cat Name' label and the New Cat and Add buttons.
        let w = UIScreen.main.bounds.size.width
        let ls = (UIScreen.main.bounds.size.height-x-40-UIScreen.main.bounds.size.width-20)/2 //Label space (Label is 20 high).
        let lbs = (ls-40)/2 //Label button space. 40 is the height of the button.
        
        //Vertical constraints for adding new cat name.
        let verticalConstraints1 = NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(x)-[h(==40)]-0-[i(\(w))]-\(ls)-[l1(==20)]-\(lbs)-[b1(==40)]-\(lbs)-|", options: [], metrics: nil, views: views)
        let verticalConstraints2 = NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(x)-[h(==40)]-0-[i(\(w))]-\(ls)-[l1(==20)]-\(lbs)-[b2(==40)]-\(lbs)-|", options: [], metrics: nil, views: views)
        let verticalConstraints3 = NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(x)-[h(==40)]-0-[i(\(w))]-\(ls)-[t(==20)]-\(lbs)-[b2(==40)]-\(lbs)-|", options: [], metrics: nil, views: views)
        
        for (_,view) in views {
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        self.contentView.addConstraints(verticalConstraints1)
        self.contentView.addConstraints(verticalConstraints2)
        self.contentView.addConstraints(verticalConstraints3)
    }
}

extension ContainerViewController {
    
    func createConstraints(_ col: UIView, head: UIView) -> Void
    {   //Distance between header and top of screen based on whether it's running on an iPhone X or not.
        var x: Int = 0
        if UIScreen.main.bounds.size.height == 812 {
            x = 40
        }
        
        //Views to add constraints to
        let views = Dictionary(dictionaryLiteral: ("c",col),("h",head))
        //Horizontal constraints
        let horizontalConstraintsC = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[c]-0-|", options: [], metrics: nil, views: views)
        let horizontalConstraintsH = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[h]-0-|", options: [], metrics: nil, views: views)
        self.view.addConstraints(horizontalConstraintsC)
        self.view.addConstraints(horizontalConstraintsH)
        
        //Vertical constraints
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(x)-[h(==40)]-0-[c]-0-|", options: [], metrics: nil, views: views)
        self.view.addConstraints(verticalConstraints)
    }
}

extension tableViewCellCategory {
    func createConstraints() -> Void
    {   //Views to add constraints to
        let views = Dictionary(dictionaryLiteral: ("label",self.textLabelCustom))
        
        //Horizontal constraints
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[label]-5-|", options: [], metrics: nil, views: views)
        self.contentView.addConstraints(horizontalConstraints)

        //Vertical constraints
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-6-[label]-6-|", options: [], metrics: nil, views: views)
        self.contentView.addConstraints(verticalConstraints)
    }
}
