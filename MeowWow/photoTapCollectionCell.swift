//
//  photoTapCollectionCell.swift
//  MeowWow
//
//  Created by Callum Morris on 22/08/15.
//  Copyright (c) 2015 Callum Morris. All rights reserved.
//

//import Foundation
import UIKit

class photoTapCollectionCell : UICollectionViewCell {
    
    var stickerView: UIImageView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let cellwidth = self.frame.width
        self.stickerView = UIImageView(frame: CGRect(x: 0, y: 0, width: cellwidth, height: cellwidth))
        stickerView.contentMode = UIViewContentMode.scaleAspectFit
        stickerView.clipsToBounds = true
        stickerView.backgroundColor = UIColor.white
        contentView.addSubview(stickerView)
    }
}
