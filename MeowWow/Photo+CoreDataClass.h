//
//  Photo+CoreDataClass.h
//  MeowWowTest
//
//  Created by CallumMorris on 24/07/17.
//  Copyright © 2017 Callum Morris. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Cat;

NS_ASSUME_NONNULL_BEGIN

@interface Photo : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Photo+CoreDataProperties.h"
