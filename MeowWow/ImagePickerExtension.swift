//
//  ImagePickerExtension.swift
//  MeowWow
//
//  Created by CallumMorris on 20/12/17.
//  Copyright © 2017 Callum Morris. All rights reserved.
//

import Foundation
import UIKit

extension UIImagePickerController {
    open override var childViewControllerForStatusBarHidden: UIViewController? {
        return nil
    }
    
    open override var prefersStatusBarHidden: Bool {
        return true
    }
}
