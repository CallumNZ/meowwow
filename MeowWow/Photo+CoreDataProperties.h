//
//  Photo+CoreDataProperties.h
//  MeowWowTest
//
//  Created by CallumMorris on 24/07/17.
//  Copyright © 2017 Callum Morris. All rights reserved.
//
//

#import "Photo+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Photo (CoreDataProperties)

+ (NSFetchRequest<Photo *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *dateCreated;
@property (nullable, nonatomic, retain) NSData *photoData;
@property (nullable, nonatomic, retain) Cat *cat;

@end

NS_ASSUME_NONNULL_END
