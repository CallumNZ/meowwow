//
//  Cat+CoreDataClass.h
//  MeowWowTest
//
//  Created by CallumMorris on 24/07/17.
//  Copyright © 2017 Callum Morris. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Photo;

NS_ASSUME_NONNULL_BEGIN

@interface Cat : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Cat+CoreDataProperties.h"
