//
//  myCatsFlowLayout.swift
//  MeowWow
//
//  Created by Callum Morris on 12/10/14.
//  Copyright (c) 2014 Callum Morris. All rights reserved.
//

import Foundation
import UIKit

///////////////iOS 9 has a built-in property to allow sticky headers, so this subclass is no longer needed.//////////////
let stickyHeaderZIndex = 100

class myCatsFlowLayout: UICollectionViewFlowLayout {
    
    var stickyHeaderIndexPaths = Array<IndexPath>()
    
    override func prepare() {
        super.prepare()
        
        stickyHeaderIndexPaths.removeAll(keepingCapacity: true)
        let numberOfSections = collectionView!.numberOfSections
        
        for index in 0 ..< numberOfSections {
            let indexPath = IndexPath(item: 0, section: index)
            stickyHeaderIndexPaths.append(indexPath)
        }
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
        //This updates the layout when view is scrolled through.
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes] {
        let attributesInRect = super.layoutAttributesForElements(in: rect)
        var updatedAttributesInRect = Array<UICollectionViewLayoutAttributes>()
        
        for obj: AnyObject in attributesInRect! {
            let attribute = obj as! UICollectionViewLayoutAttributes
            let representedElementKind = attribute.representedElementKind 
            
            if (representedElementKind == nil || representedElementKind != UICollectionElementKindSectionHeader) {
                updatedAttributesInRect.append(attribute)
            }
        }
        for indexPath in stickyHeaderIndexPaths {
            let headerAttribute = layoutAttributesForSupplementaryView(ofKind: UICollectionElementKindSectionHeader, at: indexPath)
            updatedAttributesInRect.append(headerAttribute)
        }
        return updatedAttributesInRect
    }
    
    override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes {
        let layoutAttribute: UICollectionViewLayoutAttributes! = super.layoutAttributesForSupplementaryView(ofKind: elementKind, at: indexPath)
        let contentOffset = collectionView!.contentOffset
        var nextHeaderOrigin:CGPoint = CGRect.infinite.origin
        var nextHeaderIndex: Int
        
        if ((indexPath as NSIndexPath).section + 1 < collectionView!.numberOfSections) {
            nextHeaderIndex = (indexPath as NSIndexPath).section + 1
            let nextIndexPath: IndexPath = IndexPath(item: 0, section: nextHeaderIndex)
            let nextHeaderFrame : CGRect = super.layoutAttributesForSupplementaryView(ofKind: elementKind, at: nextIndexPath)!.frame
            nextHeaderOrigin = nextHeaderFrame.origin
        }
        else {
            return layoutAttribute       //Original code where last header wasn't sticking. Have put back in, not sure if works.
        }
        
        var headerFrame: CGRect = layoutAttribute.frame
        if (scrollDirection == UICollectionViewScrollDirection.vertical) {
            let nextStickyCellY: CGFloat = nextHeaderOrigin.y - headerFrame.size.height
            let currentStickyCellY: CGFloat = max(contentOffset.y, headerFrame.origin.y)
            headerFrame.origin.y = min(currentStickyCellY, nextStickyCellY)
        }
        else {
            let nextStickyCellX: CGFloat = nextHeaderOrigin.x - headerFrame.size.width
            let currentStickyCellX = max(contentOffset.x, headerFrame.origin.x)
            headerFrame.origin.x = min(currentStickyCellX, nextStickyCellX)
        }
        
        layoutAttribute.zIndex = stickyHeaderZIndex
        layoutAttribute.frame = headerFrame
        return layoutAttribute
    }
    
}
