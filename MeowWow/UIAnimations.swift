//
//  UIAnimations.swift
//  MeowWow
//
//  Created by CallumMorris on 28/10/17.
//  Copyright © 2017 Callum Morris. All rights reserved.
//

import Foundation
import UIKit
import UIImageViewModeScaleAspect

extension photoTapCollectionVC {
    
    func offscreenPositionCollectionVC() {
        self.collectionView?.frame.origin.y += (self.collectionView?.frame.height)!
    }
    
    func animateOpenCollectionVC() {
        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveLinear, animations: {
            self.collectionView?.frame.origin.y -= (self.collectionView?.frame.height)!
        },
                       completion: nil
        )
    }
    
    func animateCloseCollectionVC() {
        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveLinear, animations: {
            self.collectionView?.frame.origin.y += (self.collectionView?.frame.height)!
        },
                       completion: {finished in
                                    self.willMove(toParentViewController: nil)
                                    self.view.removeFromSuperview()
                                    self.removeFromParentViewController()
                                    }
        )
    }
}


extension MyCatsViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        //Transition is of type Animator.
        transition.originContainer = source as? MyCatsViewController
        transition.presentedContainer = presented as? photoTapContainerVC
        
        transition.originFrame = selectedImage!.superview!.convert(selectedImage!.frame, to: nil)
        transition.presentingFrame = transition.presentedContainer!.returnPhotoFrame()
        transition.selectedPhoto = selectedImage!.image
        transition.presenting = true
    
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let currentIndex = transition.presentedContainer!.returnPhotoLinearIndex()
        transition.originFrame = returnMCVCphotoPosition(linearIndex: currentIndex)
        transition.selectedPhoto = transition.presentedContainer!.returnCurrentPhoto()
        transition.presenting = false
        
        return transition
    }
    
    //Returns position of photo on MCVC given a linear indexPath from photoTapContainerVC. Scrolls screen if photo not visible.
    func returnMCVCphotoPosition(linearIndex: IndexPath) -> CGRect {
        let library = PhotoLibrary()
        let MCVC = transition.originContainer!
        let index = library.indexPathForLinearIndexPath(linearIndex, catsArray: MCVC.myCats)
        var cell = MCVC.collectionView?.cellForItem(at: index) //is nil if cell is not visible.
        
        if cell == nil {
            MCVC.collectionView?.scrollToItem(at: index, at: .bottom, animated: false)
            MCVC.view.layoutIfNeeded()
        }
            
        else if MCVC.collectionView?.bounds.contains(cell!.frame) == false { //Tests if the cell is only half visible.
            MCVC.collectionView?.scrollToItem(at: index, at: .bottom, animated: false)
            MCVC.view.layoutIfNeeded()
        }
        
        cell = MCVC.collectionView!.cellForItem(at: index) //redefines cell after moving it completely onscreen. This ensures it's not nil.
        let onscreenFrame = cell!.superview!.convert(cell!.frame, to: cell!.superview?.superview)
        return onscreenFrame
    }
}
        
        

