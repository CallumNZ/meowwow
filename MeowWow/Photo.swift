//
//  Photo.swift
//  MeowWow
//
//  Created by Callum Morris on 1/10/14.
//  Copyright (c) 2014 Callum Morris. All rights reserved.
//

import Foundation
import UIKit

struct Photo { //This used to have the full resolution image as a property, but it's not needed now as a Core Data fetch controller is used to grab those images in photoTapViewController3. This Photo class is instead used to store thumbnails for myCatViewController.
    
    let catName: String
    let photoDate: NSDate
    let thumbnail: Data
 
    lazy var thumb: UIImage? = {
        return UIImage(data: self.thumbnail)
    }()
    
    init(cname: String, pdate: NSDate, pthumb: Data){
        self.catName = cname
        self.photoDate = pdate
        self.thumbnail = pthumb
    }
}
