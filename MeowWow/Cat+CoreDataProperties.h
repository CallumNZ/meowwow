//
//  Cat+CoreDataProperties.h
//  MeowWowTest
//
//  Created by CallumMorris on 24/07/17.
//  Copyright © 2017 Callum Morris. All rights reserved.
//
//

#import "Cat+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Cat (CoreDataProperties)

+ (NSFetchRequest<Cat *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *catName;
@property (nullable, nonatomic, retain) NSOrderedSet<Photo *> *photos;

@end

@interface Cat (CoreDataGeneratedAccessors)

- (void)insertObject:(Photo *)value inPhotosAtIndex:(NSUInteger)idx;
- (void)removeObjectFromPhotosAtIndex:(NSUInteger)idx;
- (void)insertPhotos:(NSArray<Photo *> *)value atIndexes:(NSIndexSet *)indexes;
- (void)removePhotosAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInPhotosAtIndex:(NSUInteger)idx withObject:(Photo *)value;
- (void)replacePhotosAtIndexes:(NSIndexSet *)indexes withPhotos:(NSArray<Photo *> *)values;
- (void)addPhotosObject:(Photo *)value;
- (void)removePhotosObject:(Photo *)value;
- (void)addPhotos:(NSOrderedSet<Photo *> *)values;
- (void)removePhotos:(NSOrderedSet<Photo *> *)values;

@end

NS_ASSUME_NONNULL_END
