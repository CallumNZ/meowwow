//
//  Photo+CoreDataProperties.m
//  MeowWowTest
//
//  Created by CallumMorris on 24/07/17.
//  Copyright © 2017 Callum Morris. All rights reserved.
//
//

#import "Photo+CoreDataProperties.h"

@implementation Photo (CoreDataProperties)

+ (NSFetchRequest<Photo *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Photo"];
}

@dynamic dateCreated;
@dynamic photoData;
@dynamic cat;

@end
