//
//  Utility.swift
//  MeowWow
//
//  Created by Callum Morris on 9/05/16.
//  Copyright © 2016 Callum Morris. All rights reserved.
//

import Foundation

//Bunch of global variables to make it easier to use GCD.

var GlobalMainQueue: DispatchQueue {
    return DispatchQueue.main
}

var GlobalUserInteractiveQueue: DispatchQueue { //changed .value to .rawValue for these four.
    //return DispatchQueue.global(priority: Int(DispatchQoS.QoSClass.userInteractive.rawValue))
    return DispatchQueue.global(qos: DispatchQoS.QoSClass.userInteractive)
}

var GlobalUserInitiatedQueue: DispatchQueue {
    //return DispatchQueue.global(priority: Int(DispatchQoS.QoSClass.userInitiated.rawValue))
    return DispatchQueue.global(qos: DispatchQoS.QoSClass.userInteractive)
}

var GlobalUtilityQueue: DispatchQueue {
    //return DispatchQueue.global(priority: Int(DispatchQoS.QoSClass.utility.rawValue))
    return DispatchQueue.global(qos: DispatchQoS.QoSClass.userInteractive)
}

var GlobalBackgroundQueue: DispatchQueue {
    //return DispatchQueue.global(priority: Int(DispatchQoS.QoSClass.background.rawValue))
    return DispatchQueue.global(qos: DispatchQoS.QoSClass.userInteractive)
}

class Utils {
    /*class var defaultBackgroundColor: UIColor {
        return UIColor(red: 236.0/255.0, green: 254.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    }
    
    class var userInterfaceIdiomIsPad: Bool {
        return UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad
    }*/
}
