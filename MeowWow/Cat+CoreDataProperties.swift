//
//  Cat+CoreDataProperties.swift
//  MeowWowTest
//
//  Created by CallumMorris on 25/07/17.
//  Copyright © 2017 Callum Morris. All rights reserved.
//
//

import Foundation
import CoreData


extension CatCD {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CatCD> {
        return NSFetchRequest<CatCD>(entityName: "CatCD")
    }
    
    //The @NSManaged attribute is required for Core Data to do its work. It tells the compiler that the storage and implementation of these properties
    //will be provided at runtime.
    @NSManaged public var catName: String?
    @NSManaged public var photos: NSOrderedSet?

}

// MARK: Generated accessors for photos
extension CatCD {

    @objc(insertObject:inPhotosAtIndex:)
    @NSManaged public func insertIntoPhotos(_ value: PhotoCD, at idx: Int)

    @objc(removeObjectFromPhotosAtIndex:)
    @NSManaged public func removeFromPhotos(at idx: Int)

    @objc(insertPhotos:atIndexes:)
    @NSManaged public func insertIntoPhotos(_ values: [PhotoCD], at indexes: NSIndexSet)

    @objc(removePhotosAtIndexes:)
    @NSManaged public func removeFromPhotos(at indexes: NSIndexSet)

    @objc(replaceObjectInPhotosAtIndex:withObject:)
    @NSManaged public func replacePhotos(at idx: Int, with value: PhotoCD)

    @objc(replacePhotosAtIndexes:withPhotos:)
    @NSManaged public func replacePhotos(at indexes: NSIndexSet, with values: [PhotoCD])

    @objc(addPhotosObject:)
    @NSManaged public func addToPhotos(_ value: PhotoCD)

    @objc(removePhotosObject:)
    @NSManaged public func removeFromPhotos(_ value: PhotoCD)

    @objc(addPhotos:)
    @NSManaged public func addToPhotos(_ values: NSOrderedSet)

    @objc(removePhotos:)
    @NSManaged public func removeFromPhotos(_ values: NSOrderedSet)

}
