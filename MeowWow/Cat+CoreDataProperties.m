//
//  Cat+CoreDataProperties.m
//  MeowWowTest
//
//  Created by CallumMorris on 24/07/17.
//  Copyright © 2017 Callum Morris. All rights reserved.
//
//

#import "Cat+CoreDataProperties.h"

@implementation Cat (CoreDataProperties)

+ (NSFetchRequest<Cat *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Cat"];
}

@dynamic catName;
@dynamic photos;

@end
