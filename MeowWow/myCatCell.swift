//
//  myCatCell.swift
//  MeowWow
//
//  Created by Callum Morris on 5/10/14.
//  Copyright (c) 2014 Callum Morris. All rights reserved.
//

import UIKit

class myCatCell: UICollectionViewCell {
    
    var imageView: UIImageView! 
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let width = self.frame.width
        imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: width))
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        contentView.addSubview(imageView)
}
}
    
    

