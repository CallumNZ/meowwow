//
//  PhotoLibrary.swift
//  MeowWow
//
//  Created by Callum Morris on 8/02/15.
//  Copyright (c) 2015 Callum Morris. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class PhotoLibrary: NSObject {
    
    var myCats: Array<Array<Photo>>!
    var catNames = [String]()
    
    override init (){
        super.init()
    }
        
    func photoForIndexPath(_ indexPath: IndexPath, catsArray: Array<Array<Photo>>) -> Photo {
        //Convenience method for retrieving photo from myCats at specified linear index path.
        var section: Int = 0
        var row: Int = (indexPath as NSIndexPath).row
        while row > catsArray[section].count-1 {
            row -= catsArray[section].count
            section += 1
        }
        return catsArray[section][row]
    }
    
    func indexPathForLinearIndexPath(_ indexPath: IndexPath, catsArray: Array<Array<Photo>>) -> IndexPath {
        //Convenience method for retrieving photo from myCats at specified linear index path.
        var section: Int = 0
        var row: Int = (indexPath as NSIndexPath).row
        while row > catsArray[section].count-1 {
            row -= catsArray[section].count
            section += 1
        }
        let index: IndexPath = IndexPath(row: row, section: section)
        return index
    }
    
    func linearIndexPath(_ indexPath: IndexPath, catsArray: Array<Array<Photo>>) -> IndexPath {//Makes linear indexPath out of sectioned indexPath.
        
        var x: Int = (indexPath as NSIndexPath).section
        var n: Int = 0
        var linear: Int = 0
        while x > 0 {
            linear += catsArray[n].count
            x -= 1
            n += 1
        }
        linear += (indexPath as NSIndexPath).row
        return IndexPath(item: linear, section: 0)
    }
    
    func savePhoto (_ catName: String, photo: UIImage, photoDate: NSDate) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        //Convert the UIImage into NSData, and create the thumbnail as NSData.
        let photoData = UIImageJPEGRepresentation(photo, 1)
        let thumbnail = UIImageJPEGRepresentation(photo, 0)
        
        //Make photo to put in Core Data.
        let entity =  NSEntityDescription.entity(forEntityName: "PhotoCD", in:managedContext)
        let photo = PhotoCD(entity: entity!, insertInto: managedContext)
        
        //Set the attributes.
        photo.setValue(photoData, forKey: "photoData")
        photo.setValue(photoDate, forKey: "dateCreated")
        photo.setValue(thumbnail, forKey: "thumbnail")
        
        //Find out if it's a new or existing cat, then makes a new cat entity if the former.
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CatCD")
        let predicate = NSPredicate(format: "catName == %@", catName)
        fetchRequest.predicate = predicate
        var cat: CatCD
        do {
            let results = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
            if results.count == 0 { //Create new cat entity.
                let catEntity = NSEntityDescription.entity(forEntityName: "CatCD", in:managedContext)
                cat = CatCD(entity: catEntity!, insertInto: managedContext)
                cat.setValue(catName, forKey: "catName")
                cat.setValue(NSOrderedSet(object: photo), forKey: "photos") //Adds new photo to new cat (forKey is the relationship).
            }
            else { //Defines the existing cat entity as 'cat'.
                cat = results.first! as! CatCD
                cat.setValue(catName, forKey: "catName")
                cat.addToPhotos(photo)
            }
        } catch let error as NSError {
                print("Could not execute fetch request \(error), \(error.userInfo)") }
        
        do {
            try managedContext.save()
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    func deletePhoto(photoDate: NSDate, catName: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
       //Creates fetch request. This looks at all Photo entities.
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "PhotoCD")
        
        //Add predicates
        let p1 = NSPredicate(format: "%K == %@", "cat.catName", catName) //cat is the relationship.
        let p2 = NSPredicate(format: "%K == %@", "dateCreated", photoDate)
        let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [p1, p2])
        fetchRequest.predicate = predicate
        
        //Runs the fetch request, then deletes the entity that is found.
        do {
            let results = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
            for photo in results { //There should be just one photo in the results array.
                managedContext.delete(photo)
            }
        } catch let error as NSError {
            print("Could not execute fetch request \(error), \(error.userInfo)")
        }
        
        //Determines whether that was the last photo for that cat.
        let catRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CatCD")
        let p3 = NSPredicate(format: "%K == %@", "catName", catName)
        catRequest.predicate = p3
        //Runs the fetch request, then deletes the cat if it only had one photo left.
        do {
            let results = try managedContext.fetch(catRequest) as! [CatCD]
            let cat = results.first!
            if cat.photos?.count == 0 {
                managedContext.delete(cat)
            }
        } catch let error as NSError {
            print("Could not execute fetch request \(error), \(error.userInfo)")
        }
        
        //Saves the managed context.
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    func getPhotos () -> Bool { //This gets just the thumbnail data, plus catName and date, and puts it into the myCats array.
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        //Fetches all cats
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CatCD")
        let sortDescriptor = NSSortDescriptor(key: "catName", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do {
            let results = try managedContext.fetch(fetchRequest)
            if results.count == 0 {
                self.myCats = []
                return false
            }
            var counter: Int = 0
            for cat in results as! [CatCD] {
                let photos = cat.photos
                for photo in photos?.array as! [PhotoCD] { //Turns NSOrderedSet into an array.
                    let thumbData = photo.value(forKey: "thumbnail") as? Data
                    let catName = cat.catName!
                    let photoDate = photo.value(forKey: "dateCreated") as? NSDate
                    let picture: Photo = Photo(cname: catName, pdate: photoDate!, pthumb: thumbData!)
                    let array: Array<Photo> = [picture]
                    
                    if self.myCats == nil {
                        self.myCats = [array]
                        self.catNames.append(catName)
                        continue
                    }
                    
                    if !self.catNames.contains(catName) { //if catNames array doesn't contain this cat, add it, then add new array with photo to myCats.
                        self.catNames.append(catName)
                        self.myCats.append(array)
                        counter += 1
                    }
                    else {
                        self.myCats[counter].append(picture)
                    }
                }
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return true
    }
    
    //Returns myCats array for MyCatsViewControllerInitialisation
    func returnCats() -> Array<Array<Photo>>? {
        if self.myCats == nil {return nil}
        return self.myCats
    }
    
    func missyDefault() -> Array<Array<Photo>> { //Returns pictures of Missy if the user has no photos added yet.
        let missyArray = ["Missy1.JPG","Missy2.JPG","Missy3.JPG","Missy4.JPG","Missy5.JPG","Missy6.JPG","Missy7.JPG","Missy8.JPG","Missy9.JPG","Missy10.JPG"]
        var missyPhotoArray: Array<Photo> = []
        
        for name in missyArray {
            let image: UIImage = UIImage(named: name)!
            let thumbnail: Data = UIImageJPEGRepresentation(image, 0)!
            let date: NSDate = NSDate()
            let photo: Photo = Photo(cname: "Missy", pdate: date, pthumb: thumbnail)
            missyPhotoArray.append(photo)
        }
        
        let array: Array<Array<Photo>> = [missyPhotoArray]
        return array
    }
}
