//
//  buttonBarVC.swift
//  MeowWow
//
//  Created by Callum Morris on 8/01/16.
//  Copyright © 2016 Callum Morris. All rights reserved.
//

import UIKit

protocol buttonDelegate: NSObjectProtocol {
    
    func dismissSelected(_ controller: buttonBarVC)
    func cancelSelected(_ controller: buttonBarVC)
    func saveSelected(_ controller: buttonBarVC)
    func lockSelected(_ controller: buttonBarVC)
}

class buttonBarVC: UIViewController {

    weak var delegate: buttonDelegate?
    var buttonList: Array<UIButton>!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor(white: 0.9, alpha: 1.0) //grey colour.
        
        //Creating and adding dismiss, cancel, save, and lock buttons.
        let buttons = self.makeButtons() //this is a tuple.
        
        //Making button frames based on how big buttonBar is.
        let barL = self.view.frame.width
        buttons.cancelB.frame = CGRect(x: barL-50, y: 0, width: 50, height: 30)
        buttons.saveB.frame = CGRect(x: barL-115, y: 0, width: 50, height: 30)
        buttons.dismissB.frame = CGRect(x: 0, y: 0, width: 50, height: 30)
        buttons.lockB.frame = CGRect(x: barL-180, y: 0, width: 50, height: 30)

        self.view.addSubview(buttons.cancelB)
        self.view.addSubview(buttons.saveB)
        self.view.addSubview(buttons.dismissB)
        self.view.addSubview(buttons.lockB)
    }
    
    func makeButtons() -> (dismissB: UIButton, cancelB: UIButton, saveB: UIButton, lockB: UIButton)
    {
        //Making cancel button to show on bar when sticker is selected (along with save).
        let cancelButton = UIButton(type: UIButtonType.system)
        cancelButton.backgroundColor = UIColor.clear
        cancelButton.setBackgroundImage(UIImage(named: "CancelButton.png"), for: UIControlState())
        cancelButton.setBackgroundImage(UIImage(named: "CancelButtonOff.png"), for: UIControlState.disabled)
        cancelButton.addTarget(self, action: #selector(buttonBarVC.cancelButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        cancelButton.isUserInteractionEnabled = false //initially greyed out and uninteractable.
        cancelButton.isEnabled = false
        
        //Making save (ie: share) button.
        let saveButton = UIButton(type: UIButtonType.system)
        saveButton.backgroundColor = UIColor.clear
        saveButton.setBackgroundImage(UIImage(named: "ShareButton.png"), for: UIControlState())
        saveButton.setBackgroundImage(UIImage(named: "ShareButtonOff.png"), for: UIControlState.disabled)
        saveButton.addTarget(self, action: #selector(buttonBarVC.saveButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        saveButton.isUserInteractionEnabled = false //initially greyed out and uninteractable.
        saveButton.isEnabled = false
        
        //Making dismiss button.
        let dismissButton = UIButton(type: UIButtonType.system)
        dismissButton.backgroundColor = UIColor.clear
        dismissButton.setBackgroundImage(UIImage(named: "DismissButton.png"), for: UIControlState())
        dismissButton.setBackgroundImage(UIImage(named: "DismissButtonOff.png"), for: UIControlState.disabled)
        dismissButton.addTarget(self, action: #selector(buttonBarVC.dismissButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        dismissButton.isUserInteractionEnabled = false //initially greyed out and uninteractable.
        dismissButton.isEnabled = false
        
        //Making lock button
        let lockButton = UIButton(type: UIButtonType.system)
        lockButton.backgroundColor = UIColor.clear
        lockButton.setBackgroundImage(UIImage(named: "LockButton.png"), for: UIControlState())
        lockButton.setBackgroundImage(UIImage(named: "LockButtonOff.png"), for: UIControlState.disabled)
        lockButton.addTarget(self, action: #selector(buttonBarVC.lockButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        lockButton.isUserInteractionEnabled = false //initially greyed out and uninteractable.
        lockButton.isEnabled = false
        
        //Add buttons to property list ([0],[1],[2])
        self.buttonList = [dismissButton, cancelButton, saveButton, lockButton]
        
        return (dismissButton, cancelButton, saveButton, lockButton) //Returns a tuple containing the 4 buttons.
    }

    @objc func cancelButtonTapped(_ sender: UIButton!) {
        if let d = self.delegate {
            d.cancelSelected(self)
        }
    }
    
    @objc func saveButtonTapped(_ sender: UIButton!) {
        if let d = self.delegate {
            d.saveSelected(self)
        }
    }
    
    @objc func dismissButtonTapped(_ sender: UIButton!) {
        if let d = self.delegate {
            d.dismissSelected(self)
        }
    }
    
    @objc func lockButtonTapped(_ sender: UIButton!) {
        if let d = self.delegate {
            d.lockSelected(self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
