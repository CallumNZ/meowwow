//
//  MyCatsViewController.swift
//  MeowWow
//
//  Created by Callum Morris on 28/09/14.
//  Copyright (c) 2014 Callum Morris. All rights reserved.
//

import UIKit

class MyCatsViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {

    var collectionView: UICollectionView?
    var myCats: Array<Array<Photo>>!
    let transition = Animator()
    var selectedImage: UIImageView?
    
    //Convenice method for retrieving the names of the cats in the myCats array. Used by the container view to initialise the addPhotoVC.
    func catNames() -> Array<String> {
        var names = Array<String>()
        for name in myCats {
            names.append(name[0].catName)
        }
        return names
    }
    
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, cats:Array<Array<Photo>>) {
        self.myCats = cats
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.collectionView = nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //UICollectionView setup
        let layout = UICollectionViewFlowLayout()
        layout.sectionHeadersPinToVisibleBounds = true
        collectionView = UICollectionView(frame: CGRect(), collectionViewLayout: layout)
        collectionView!.dataSource = self
        collectionView!.delegate = self
        
        //Registers class that will represent a cell in our collection view:
        collectionView!.register(myCatCell.self, forCellWithReuseIdentifier: "Cell")
        collectionView!.backgroundColor = UIColor.white
        collectionView!.scrollIndicatorInsets = .zero
        self.view.addSubview(collectionView!)
        
        //Registers class that will represent a header in our collection view:
        collectionView!.register(MyCatsNameHeader.self, forSupplementaryViewOfKind:UICollectionElementKindSectionHeader, withReuseIdentifier: "CatName")
        
        //Layout setup
        let spacer:CGFloat = 5
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        layout.minimumInteritemSpacing = spacer
        layout.minimumLineSpacing = spacer
        //Item size is got using delegate method below :)
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions LaunchOptions:NSDictionary?) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
        //Gets linear indexPath
        let pl: PhotoLibrary = PhotoLibrary()
        let linearIndexPath: IndexPath = pl.linearIndexPath(indexPath, catsArray: self.myCats)

        //Displays photoTapContainer view, which contains the photo selected and a table for selecting stickers.
        let pTCVC = photoTapContainerVC()
        pTCVC.CIP = linearIndexPath
        pTCVC.myCats = self.myCats
        pTCVC.delegate = self.parent as! ContainerViewController
        
        //A reference to the last selected cell is used to help animate selection and dismissal.
        let selectedCell = self.collectionView?.cellForItem(at: indexPath) as! myCatCell
        self.selectedImage = selectedCell.imageView
        
        pTCVC.transitioningDelegate = self
        self.present(pTCVC, animated: true, completion: nil)
    }
    
    ////////////////////////////////
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myCats[section].count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return myCats.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.view.frame.width-20)/3
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! myCatCell
        cell.imageView.image = myCats[(indexPath as NSIndexPath).section][(indexPath as NSIndexPath).row].thumb
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: self.view.frame.width, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
    
        var reusableView: UICollectionReusableView?
        
        if (kind == UICollectionElementKindSectionHeader) {
            let headerView: MyCatsNameHeader = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "CatName", for: indexPath) as! MyCatsNameHeader
            
            headerView.catName!.text = myCats[(indexPath as NSIndexPath).section][(indexPath as NSIndexPath).row].catName
            headerView.layer.zPosition = 0
            reusableView = headerView
        }
        return reusableView!
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let verticalIndicator = scrollView.subviews.last as? UIImageView
        verticalIndicator?.layer.zPosition = 10
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

