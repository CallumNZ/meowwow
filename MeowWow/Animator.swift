//
//  Animator.swift
//  MeowWow
//
//  Created by CallumMorris on 21/12/17.
//  Copyright © 2017 Callum Morris. All rights reserved.
//

import UIKit
import UIImageViewModeScaleAspect

class Animator: NSObject, UIViewControllerAnimatedTransitioning {
    
    let duration = 0.3
    var presenting = true
    var dismissCompletion: (()->Void)?
    
    var originContainer: MyCatsViewController?
    var presentedContainer: photoTapContainerVC?
    
    //Frame and photo of the photo selected in MyCatsViewController. Properties changed in MyCatsViewController before presentation. On dismissal, properties are changed if a new photo has been scrolled to in photoTapContainerVC.
    var originFrame = CGRect.zero //small
    var presentingFrame = CGRect.zero //large
    var selectedPhoto: UIImage?

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        //toView is the the end view in the transition. The animated view is always the presented container, so can be the 'to' or the 'from' view.
        let transitionContainer = transitionContext.containerView
        let toView = transitionContext.view(forKey: .to)!
        let animatedView = presenting ? toView :
            transitionContext.view(forKey: .from)!
        
        //Creates photo that gets bigger to fill screen when selected, or smaller to go back to MyCatsViewController. Default frame is small for init.
        let animatedPhoto = UIImageViewModeScaleAspect(frame: originFrame)
        animatedPhoto.image = selectedPhoto
        animatedPhoto.backgroundColor = UIColor.white
        var finalFrame = presentedContainer?.returnPhotoFrame()
        if !presenting {
            animatedPhoto.frame = finalFrame!
            finalFrame = originFrame
        }
        
        transitionContainer.addSubview(toView)
        transitionContainer.bringSubview(toFront: animatedView)
        transitionContainer.addSubview(animatedPhoto)
                
        //Uses the UIImageViewModeScaleAspect library
        if presenting {
            animatedPhoto.contentMode = .scaleAspectFill
            animatedPhoto.initialState(.fit, newFrame: finalFrame!)
            
            presentedContainer?.createConstraintsForTransition()
            presentedContainer?.view.layoutIfNeeded()
            presentedContainer?.createConstraints()
        }
        else {
            animatedPhoto.contentMode = .scaleAspectFit
            animatedPhoto.initialState(.fill, newFrame: finalFrame!)
            presentedContainer?.hidePhoto(hide: true)
            presentedContainer?.createConstraintsForTransition()
        }
        
        UIView.animate(withDuration: duration, delay:0.0, options: .curveLinear, animations: {
                        
                        if self.presenting {
                            animatedPhoto.transitionState(.fit)
                            self.presentedContainer?.view.layoutIfNeeded()
                        } else {
                            animatedPhoto.transitionState(.fill)
                            self.presentedContainer?.view.layoutIfNeeded()
                        }
        },
                       completion: { _ in
                        if self.presenting {
                            self.presentedContainer?.hidePhoto(hide: false)
                            animatedPhoto.endState(.fit)
                        } else {
                            self.dismissCompletion?()
                            animatedPhoto.endState(.fill)
                        }
                        animatedPhoto.removeFromSuperview()
                        transitionContext.completeTransition(true)
        }
        )
    }
}
