//
//  photoTapViewCell2.swift
//  MeowWow
//
//  Created by Callum Morris on 26/03/15.
//  Copyright (c) 2015 Callum Morris. All rights reserved.
//

import Foundation
import UIKit

//This sets up the UICollectionViewCell.
class photoTapViewCell2 : UICollectionViewCell{
    
    var photoFrame: UIImageView!
    var pullAction : ((_ offset : CGPoint) -> Void)?
    var tappedAction : (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //Adding photoframe. This should cover the whole cell.
        let screen = self.frame.width
        
        photoFrame = UIImageView(frame: CGRect(x: 0, y: 0, width: screen, height: screen))
        photoFrame.contentMode = UIViewContentMode.scaleAspectFit
        photoFrame.backgroundColor = UIColor.white
        self.contentView.addSubview(photoFrame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func prepareForReuse() {
        self.photoFrame.image = nil
        super.prepareForReuse()
    }
}

