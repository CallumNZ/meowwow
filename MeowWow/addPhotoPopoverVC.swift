//
//  addPhotoPopoverVC.swift
//  MeowWow
//
//  Created by CallumMorris on 28/08/17.
//  Copyright © 2017 Callum Morris. All rights reserved.
//

import UIKit

protocol popoverOptions: NSObjectProtocol {
    func takePhotoTapped()
    func chooseFromLibraryTapped()
}

class addPhotoPopoverVC: UIViewController {
    
    weak var delegate: popoverOptions?

    override func viewDidLoad() {
        super.viewDidLoad()
        makePopOverButtons()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func makePopOverButtons() {
        let button1 = UIButton(type: UIButtonType.system)
        let button2 = UIButton(type: UIButtonType.system)
        
        button1.frame = CGRect(x: 0, y: 0, width: 160, height: 40)
        button2.frame = CGRect(x: 0, y: 40, width: 160, height: 40)
        
        button1.setTitle("Take Photo", for: UIControlState())
        button2.setTitle("Choose from Library", for: UIControlState())
        button1.setTitleColor(UIColor.purple, for: UIControlState())
        button2.setTitleColor(UIColor.purple, for: UIControlState())

        button1.addTarget(self, action: #selector(button1Selected(_:)), for: UIControlEvents.touchUpInside)
        button2.addTarget(self, action: #selector(button2Selected(_:)), for: UIControlEvents.touchUpInside)
        self.view.addSubview(button1)
        self.view.addSubview(button2)
    }
    
    @objc func button1Selected(_ sender: UIButton) {
        self.dismiss(animated: true, completion: self.delegate?.takePhotoTapped)
    }
    @objc func button2Selected(_ sender: UIButton) {
        self.dismiss(animated: true, completion: self.delegate?.chooseFromLibraryTapped)
    }

}
