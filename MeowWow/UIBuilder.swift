//
//  UIBuilder.swift
//  MeowWow
//
//  Created by CallumMorris on 25/10/17.
//  Copyright © 2017 Callum Morris. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func makeHeader() -> UIView {
        let header = UIView()
        header.backgroundColor = UIColor.purple
        let midPoint = UIScreen.main.bounds.size.width/2
        let logoFrame = UIImageView(frame: CGRect(x: midPoint-60, y: 0, width: 120, height: 40))
        logoFrame.image = UIImage(named: "LogoHeader.png")
        logoFrame.contentMode = UIViewContentMode.scaleAspectFit
        header.addSubview(logoFrame)
        header.accessibilityIdentifier = "Header"
        return header
    }
    
    func makeiPhoneXView() -> UIView {
        //For colouring the back of the status bar when running on an iPhone X
        let view = UIView()
        view.backgroundColor = UIColor.purple
        view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40)
        return view
    }
    
    func makeAddPhotoButton() -> UIButton {
        let button = UIButton(type: UIButtonType.system)
        
        //Making button frames based on how big buttonBar is.
        let barL = UIScreen.main.bounds.size.width
        button.frame = CGRect(x: barL-100, y: 0, width: 100, height: 40)
        button.setBackgroundImage(UIImage(named: "AddPhotoButton.png"), for: UIControlState())
        button.addTarget(self, action: #selector(ContainerViewController.addPhotoButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        button.accessibilityIdentifier = "AddNewPhotoButton"
        return button
    }
    
    func makeInfoButton() -> UIButton {
        let infoButton = UIButton(type: UIButtonType.system)
        
        infoButton.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        infoButton.setBackgroundImage(UIImage(named: "InfoButton.png"), for: UIControlState())
        infoButton.addTarget(self, action: #selector(ContainerViewController.infoButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        infoButton.accessibilityIdentifier = "InfoButton"
        return infoButton
    }
    
    func makeBackButton(controller: UIViewController) -> UIButton {
        
        let backButton = UIButton(type: UIButtonType.system)
        backButton.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        backButton.setBackgroundImage(UIImage(named: "BackButton.png"), for: UIControlState())
        if controller is photoTapContainerVC {
            backButton.addTarget(self, action: #selector(photoTapContainerVC.backButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        } else if controller is addPhotoVC {
            backButton.addTarget(self, action: #selector(addPhotoVC.backButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        }
        backButton.accessibilityIdentifier = "BackButton"
        return backButton
    }
    
    func makeDeletePhotoButton() -> UIButton {
        let button = UIButton(type: UIButtonType.system)
        button.setBackgroundImage(UIImage(named: "DeleteButton.png"), for: UIControlState())
        button.setBackgroundImage(UIImage(named: "DeleteButtonOff.png"), for: UIControlState.disabled)
        button.addTarget(self, action: #selector(photoTapContainerVC.deletePhotoButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        button.accessibilityIdentifier = "DeletePhotoButton"
        return button
    }
    
    func makeAddButton() -> UIButton {
        let addButton = UIButton(type: UIButtonType.system)
        addButton.layer.cornerRadius = 20
        addButton.backgroundColor = UIColor.purple
        addButton.titleLabel!.font = UIFont(name:"Arial", size: 18)
        addButton.setTitleColor(UIColor.white, for:UIControlState())
        addButton.setTitle("Add", for: UIControlState())
        addButton.addTarget(self, action: #selector(addPhotoVC.addButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        addButton.accessibilityIdentifier = "AddPhotoButton"
        return addButton
    }
    
    func makeNewCatButton() -> UIButton {
        let newCatButton = UIButton(type: UIButtonType.system)
        newCatButton.layer.cornerRadius = 20
        newCatButton.backgroundColor = UIColor.purple
        newCatButton.titleLabel!.font = UIFont(name:"Arial", size: 18)
        newCatButton.setTitleColor(UIColor.white, for:UIControlState())
        newCatButton.setTitle("New Cat", for: UIControlState())
        newCatButton.addTarget(self, action: #selector(addPhotoVC.newCatButtonTapped(_:)), for: UIControlEvents.touchUpInside)
        newCatButton.accessibilityIdentifier = "NewCatButton"
        
        return newCatButton
    }
    
    func makeCatNameLabel() -> (UILabel) {
        let label = UILabel()
        label.font = UIFont(name: "Arial", size: 18)
        label.text = "Cat Name:"
        label.textColor = UIColor.purple
        label.accessibilityIdentifier = "CatNameLabel"
        return (label)
    }
    

    
}
