//
//  ContainerViewController.swift
//  MeowWow
//
//  Created by Callum Morris on 28/09/14.
//  Copyright (c) 2014 Callum Morris. All rights reserved.
//

import UIKit
import SafariServices

class ContainerViewController: UIViewController, UIPopoverPresentationControllerDelegate, updateDelegate, popoverOptions {
    
    @IBOutlet var scrollView: UIScrollView? //Don't use this at the moment 22Oct15.
    
    var cNames: Array<String>?
    var noPhotos: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createChildren()
    }
    
    func createChildren() {
        
        // 1) Create the My Cats view used in the container view.
        let emptyArray: Array<Array<Photo>> = []
        let myCatsVC:MyCatsViewController = MyCatsViewController(nibName: "MyCatsViewController", bundle: nil, cats: emptyArray)
        self.addChildViewController(myCatsVC)
        self.updateMCVC()
        self.cNames = myCatsVC.catNames()
        
        self.view.addSubview(myCatsVC.view)
        myCatsVC.didMove(toParentViewController: self)
        
        //Make add button/info button/header
        let header = makeHeader()
        let addButton = makeAddPhotoButton()
        let infoButton = makeInfoButton()
        self.view.addSubview(header)
        header.addSubview(addButton)
        header.addSubview(infoButton)
        
        //Make iPhoneX UIView to colour status bar (if needed)
        if UIScreen.main.bounds.size.height == 812 {
            self.view.addSubview(makeiPhoneXView())
        }
        
        //Constraints
        myCatsVC.collectionView?.translatesAutoresizingMaskIntoConstraints = false
        header.translatesAutoresizingMaskIntoConstraints = false
        self.createConstraints(myCatsVC.collectionView!, head: header)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if self.noPhotos == true {
            self.firstTimeAlert()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func infoButtonTapped(_ sender: UIButton!) {
        
        let alert = UIAlertController(title: "Purrivacy Policy", message: "Would you like to open a browser window to view the privacy policy?", preferredStyle: UIAlertControllerStyle.alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default) {(action) in
            let privacyPolicyURL = URL(string: "https://www.facebook.com/MeowWowApp/posts/1996407933997663?__tn__=-R")!
            let svc = SFSafariViewController(url: privacyPolicyURL)
            self.present(svc, animated: true, completion: nil)
        }
        
        let noAction = UIAlertAction(title: "No", style: .default) {(action) in
            return
        }
        alert.addAction(yesAction)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func addPhotoButtonTapped(_ sender: UIButton!) {
        let popoverContentController = addPhotoPopoverVC()
        popoverContentController.preferredContentSize = CGSize(width: 160, height: 80)
        popoverContentController.delegate = self
        
        // Set the presentation style to modal so that the below delegate methods get called.
        popoverContentController.modalPresentationStyle = .popover
        
        // Set the popover presentation controller delegate so that the below delegate methods get called.
        popoverContentController.popoverPresentationController!.delegate = self
        
        self.present(popoverContentController, animated: true, completion: nil)
    }
    
    //Two add photo delegate methods called by addPhotoPopoverVC:
    @objc func takePhotoTapped() {
        let addVC = addPhotoVC()
        addVC.delegate = self
        addVC.wheelData = self.cNames!
        addVC.takePhoto = true
        self.present(addVC, animated: true, completion: nil)
        
    }
    @objc func chooseFromLibraryTapped() {
        let addVC = addPhotoVC()
        addVC.delegate = self
        addVC.wheelData = self.cNames!
        addVC.takePhoto = false
        self.present(addVC, animated: true, completion: nil)
    }
    
    //UIAlert for adding a photo when app is opened for the first time, or when all photos are deleted.
    func firstTimeAlert () {
        let alert = UIAlertController(title: "Welcome to MeowWow!", message: "Add your first cat photo", preferredStyle: UIAlertControllerStyle.alert)
        let buttonAction1 = UIAlertAction(title: "Choose from Library", style: .default) {(action) in
            let addVC = addPhotoVC()
            addVC.delegate = self
            addVC.wheelData = [""]
            addVC.takePhoto = false
            self.present(addVC, animated: true, completion: nil)
        }
        let buttonAction2 = UIAlertAction(title: "Take photo with camera", style: .default) {(action2) in
            let addVC = addPhotoVC()
            addVC.delegate = self
            addVC.wheelData = [""]
            addVC.takePhoto = true
            self.present(addVC, animated: true, completion: nil)
        }
        
        alert.addAction(buttonAction1)
        alert.addAction(buttonAction2)
        self.present(alert, animated: true, completion: nil)
    }
    
    //DELEGATED METHODS:
    func updateMCVC() {
        let MCVC = self.childViewControllers[0] as! MyCatsViewController //gets first child which is the MCVC.
        let populate: PhotoLibrary = PhotoLibrary()
        
        //For when last photo deleted - reloads default Missy photos (was myCatsCD, changed 24Jul17).
        if populate.getPhotos() == false { //This means that there are no photos or cats in the database.
            self.noPhotos = true
            MCVC.myCats = populate.missyDefault()
            MCVC.collectionView?.reloadData()
            
            let indexSet = IndexSet(integer: 0) //fixes header not moving when photo deleted bug.
            MCVC.collectionView?.reloadSections(indexSet)
            self.firstTimeAlert()
            
            return
        }
        
        //If true is returned when populate.getPhotos() is called:
        MCVC.myCats = populate.returnCats()
        self.noPhotos = false
        self.cNames = MCVC.catNames()
        MCVC.collectionView?.reloadData()
    }
    
    //These two methods lock device in portrait orientation. Need to put into each view controller (the containers) that needs to be locked.
    override var shouldAutorotate : Bool {
        return false
    }
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        //(Internet answer): So I wasn't aware but in Swift 2.0 multiple bitmasks are now put in an array:
        return [UIInterfaceOrientationMask.portrait, UIInterfaceOrientationMask.portraitUpsideDown]
    }
    
    override var prefersStatusBarHidden : Bool {
        //For iPhone X
        if UIScreen.main.bounds.size.height == 812 {
            return false
        }
        return true
    }
    
    // Mark: - UIPopoverPresentationControllerDelegate
    
    func prepareForPopoverPresentation(_ popoverPresentationController: UIPopoverPresentationController) {
        popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirection.up
        popoverPresentationController.sourceView = self.view.subviews[2] //Gets the header
        let buttonFrame = self.view.subviews[2].subviews[1].frame
        popoverPresentationController.sourceRect = buttonFrame.offsetBy(dx: buttonFrame.width/2, dy: 2)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
//////////////OLD VIEWDIDLOAD WITH 3 SCREEN SNAPCHAT LAYOUT:////////////
    /*override func viewDidLoad() {
        super.viewDidLoad()
        
        // !) Create the three views used in the swipe container view
        let myCatsVC :MyCatsViewController = MyCatsViewController(nibName: "MyCatsViewController", bundle: nil)
        let mainVC: MainViewController = MainViewController(nibName: "MainViewController", bundle: nil)
        let friendsVC: FriendsViewController = FriendsViewController(nibName: "FriendsViewController", bundle: nil)
        
        // 2) Add in each view to the container view hierarchy
        //    Add them in opposite order since the view hierarchy is a stack.
        self.addChildViewController(friendsVC)
        self.scrollView!.addSubview(friendsVC.view)
        friendsVC.didMoveToParentViewController(self)
        
        self.addChildViewController(mainVC)
        self.scrollView!.addSubview(mainVC.view)
        mainVC.didMoveToParentViewController(self)
        
        self.addChildViewController(myCatsVC)
        self.scrollView!.addSubview(myCatsVC.view)
        myCatsVC.didMoveToParentViewController(self)
        
        // 3) Set up the frames of the view controllers to align
        //    with eachother inside the container view.
        var adminFrame :CGRect = myCatsVC.view.frame
        adminFrame.origin.x = adminFrame.width //Makes mainVC middle.
        mainVC.view.frame = adminFrame
        
        var BFrame :CGRect = mainVC.view.frame
        BFrame.origin.x = 2*BFrame.width //Makes friendsVC furthest to the right.
        friendsVC.view.frame = BFrame
        
        // 4) Finally set the size of the scroll view that contains the frames
        let scrollWidth: CGFloat = 3 * self.view.frame.width
        let scrollHeight: CGFloat = self.view.frame.size.height
        self.scrollView!.contentSize = CGSizeMake(scrollWidth, scrollHeight)
        
        
        // Do any additional setup after loading the view.
    }*/
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
