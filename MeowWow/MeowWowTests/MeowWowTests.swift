//
//  MeowWowTests.swift
//  MeowWowTests
//
//  Created by CallumMorris on 30/12/17.
//  Copyright © 2017 Callum Morris. All rights reserved.
//

import XCTest
@testable import MeowWow

class MeowWowTests: XCTestCase {
    
    var classUnderTest: MyCatsViewController!
    
    override func setUp() {
        super.setUp()
        
        classUnderTest = MyCatsViewController(nibName: "MyCatsViewController", bundle: nil, cats: self.missyDefault())
        
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func missyDefault() -> Array<Array<Photo>> { //Returns pictures of Missy if the user has no photos added yet.
        let missyArray = ["Missy1.JPG","Missy2.JPG","Missy3.JPG","Missy4.JPG","Missy5.JPG","Missy6.JPG","Missy7.JPG","Missy8.JPG","Missy9.JPG","Missy10.JPG"]
        var missyPhotoArray: Array<Photo> = []
        
        for name in missyArray {
            let image: UIImage = UIImage(named: name)!
            let thumbnail: Data = UIImageJPEGRepresentation(image, 0)!
            let date: NSDate = NSDate()
            let photo: Photo = Photo(cname: "Missy", pdate: date, pthumb: thumbnail)
            missyPhotoArray.append(photo)
        }
        
        let array: Array<Array<Photo>> = [missyPhotoArray]
        return array
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        classUnderTest = nil
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
