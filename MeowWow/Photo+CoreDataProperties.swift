//
//  Photo+CoreDataProperties.swift
//  MeowWowTest
//
//  Created by CallumMorris on 25/07/17.
//  Copyright © 2017 Callum Morris. All rights reserved.
//
//

import Foundation
import CoreData


extension PhotoCD {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PhotoCD> {
        return NSFetchRequest<PhotoCD>(entityName: "PhotoCD")
    }

    @NSManaged public var dateCreated: NSDate?
    @NSManaged public var photoData: NSData?
    @NSManaged public var cat: CatCD?

}
