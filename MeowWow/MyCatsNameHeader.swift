//
//  MyCatsNameHeader.swift
//  MeowWow
//
//  Created by Callum Morris on 11/10/14.
//  Copyright (c) 2014 Callum Morris. All rights reserved.
//

//import Foundation
import UIKit

class MyCatsNameHeader: UICollectionReusableView {
    
    var catName: UILabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        catName = UILabel(frame: CGRect(x: 5, y: 0, width: frame.size.width, height: frame.size.height))
        catName!.textColor = UIColor.purple
        catName!.backgroundColor = UIColor.white.withAlphaComponent(0.9) //Makes label transparent.
    
        addSubview(catName!)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    
    
    
}
