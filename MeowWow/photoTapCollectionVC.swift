//
//  photoTapCollectionVC.swift
//  MeowWow
//
//  Created by Callum Morris on 19/08/15.
//  Copyright (c) 2015 Callum Morris. All rights reserved.
//

import UIKit

//This protocol is to allow the photoTapViewController3 to be notified that a sticker has been selected. The photoTapViewController3 is set as the delegate of the photoTapCollectionVC instance.
protocol stickerDelegate: NSObjectProtocol {
    
    func stickerSelected(_ controller: photoTapCollectionVC, cellview: UIView, sticker: UIImage)
}

protocol containerDelegate: NSObjectProtocol {
    func makeDeleteSaveActive(_ binary: Bool)
    func makeDeleteButtonActive(_ binary: Bool)
}

class photoTapCollectionVC: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    weak var delegate: stickerDelegate? 
    weak var delegate2: containerDelegate?
    
    var stickerNames: Array<String>?
    
    init(collectionViewLayout layout: UICollectionViewFlowLayout!, currentIndexPath indexPath: IndexPath, frame: CGRect, category: Int){
        
        //Sets up the CollectionView and ViewFlowLayout
        super.init(collectionViewLayout:layout)
        
        self.collectionView!.frame = frame
        self.collectionView!.isPagingEnabled = true
        self.collectionView!.register(photoTapCollectionCell.self, forCellWithReuseIdentifier: "collectCell")
        self.collectionView!.isScrollEnabled = true
        self.collectionView!.backgroundColor = UIColor.white
        self.collectionView!.dataSource = self
        self.collectionView!.delegate = self
        
        //Layout setup.
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        //Item size/spacing is got using delegate methods below :)
        
        //Populate sticker library and sticker names.
        self.stickerNames = self.populateStickerNameLibrary(category)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.collectionView = nil
        self.stickerNames?.removeAll(keepingCapacity: false)
    }
 
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.offscreenPositionCollectionVC()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        delay(0, closure: {self.collectionView!.flashScrollIndicators()}) //Weird bug, but need a delay for it to work (even if it's 0 seconds).
        super.viewDidAppear(true)
        self.animateOpenCollectionVC()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.animateCloseCollectionVC()
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) { //Delay function from internet.
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: UICollectionViewDataSource
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell: photoTapCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectCell", for: indexPath) as! photoTapCollectionCell
        var picture: UIImage?
        let name: String = self.stickerNames![indexPath.row]
        if let imgPath = Bundle.main.path(forResource: name, ofType: nil) {
            picture = UIImage(contentsOfFile: imgPath)
        }
        cell.stickerView!.image = picture
        return cell
    }
    
    func populateStickerNameLibrary(_ category: Int) -> Array<String> {
        var stringArray: Array<String> = []
        switch category { //I have changed the order to match the photoTapTableVC order. It's easier just to move the numbers arround here in the cases.
        case 3: //Shoe strings
            stringArray = ["chuckTaylor.png","cowboyBoot.png","purpleBoot.png","football-shoes.png","Machovka_Shoes.png","blueShoe.png","brownBoot.png","gumboots.png","highBoots.png","highTops.png","iceskates.png","yellowShoes.png"]
        case 6: //Love strings
            stringArray = ["heartCircle.png", "LaceHeart.png", "loveWord.png","flower1.png","flower2.png","lipstickKiss.png","sketchyHearts.png","SmilingCat.png","blueButterfly.png","heartArrow.png","loveBear.png","orangeHeart.png","pinkHeart.png","rainbowHeart.png","rose.png","star2-800px.png","yellowStarHeart.png"]
        case 0: //Hat strings
            stringArray = ["crown2.png","orientalHat1.png","orientalHat2.png","blackCap.png","jewishHat.png","policeHat.png","propellorHat.png","santaHat1.png","santaHat2.png","sombrero.png","partyhat-800px.png","headphones.png","wizardHat.png","chefHat.png","adventurerHat.png","brownHat.png","graduationHat.png","hardHat.png","jesterHat.png","partyHat2.png","pimpHat.png","redTopHat.png","whiteTopHat.png","americaHat.png","blackTopHat.png","blueTopHat.png","greenTopHat.png","bobMarleyHat.png"]
        case 2: //Glasses strings
            stringArray = ["3DGlassesTest.jpg","blackGlaseswithBand.png","BlackSunglassesTest.png","futureGlasses.png","greenGlasses.png","noseAndMustache.png","redGlasses.png","dogGlasses.png","masceradeMask.png","partyMask.png","hipsterGlasses.png","blackSunglasses.png","blueGlasses.png","brownGlasses.png","orangeGlasses.png","pinkGlasses.png","purpleGlasses.png","redSunglasses.png","tanGlasses.png","ericGlasses.png"]
        case 5: //Bling strings
            stringArray = ["cash2.png","cash3.png","coinPot.png","coinStacks.png","dollarSign.png","goldcoin.png","goldPendant.png","goldWatch.png","moneyBag.png","champagne.png","lemonade.png","martini.png","bubbleTea.png","champagneBottle.png"]
        case 7: //Accessories strings
            stringArray = ["blackEye.png","blueEye.png","blueEye2.png","bowtie.png","mostache1.png","mostache2.png","necktie.png","smile.png","fangs.png","grossTeeth.png","pinkRibbon.png","redRibbon.png","sax.png","smile2.png","cigarette.png","conicalFlask.png","magnifyingGlass.png","skateboard.png","anchor.png","blood.png","pills.png","poo.png","powder.png","rabbitInHat.png","syringe.png","vGuitar.png"]
        case 1: //Hair strings
            stringArray = ["animeHair1.png","animeHair2.png","animeHair3.png","animeHair4.png","blondeHair.png","brownHair.png"]
        case 4: //Food strings
            stringArray = ["aleDrink.png","beer.png","apple.png","chilliPepper.png","dessert.png","fishbone.png","foodBowl.png","fries.png","grapes.png","lasagna.png","orange.png","pizzaSlice.png","rice.png","spaghetti.png","takeOut.png","tomato.png","waterDish.png"]
        case 8: //Sport strings
            stringArray = ["baseball.png","baseballBat.png","baseballHat.png","basketball.png","basketballHoop.png","cricketBall.png","cricketbat.png","cricketWickets.png","football.png","footballGoal.png","hockeyGoal.png","hockeyPuck.png","hockeyStick.png","rugbyBall.png","tickets.png","usFootball.png","usFootballHel1.png","usFootballHel2.png","usFootballPost.png","whistle.png"]
        default:
            print("No category found")
        }
        return stringArray
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let frameWidth = self.collectionView!.bounds.width
        let frameHeight = self.collectionView!.bounds.height
        
        //Accounts for iPhone, iPhone Plus, and iPad
        var width: CGFloat = 0
        if frameHeight/2 * 3 > frameWidth {
            //is iPhone Plus
            width = frameHeight/3
        } else {
            if UIScreen.main.bounds.size.height == 480 {
                width = frameHeight // is 3.5 inch device.
            } else {width = frameHeight/2}} //is regular iphone or iPad

        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat { //I think the collectionView is displaying in columns so the 'item spacing' is actually line spacing.
        let frameWidth = self.collectionView!.bounds.width
        let frameHeight = self.collectionView!.bounds.height
        
        //Accounts for iPhone, iPhone Plus, and iPad.
        var cellWidth: CGFloat = 0
        if frameHeight/2 * 3 > frameWidth {
            //is iPhone Plus
            cellWidth = frameHeight/3
        } else {if UIScreen.main.bounds.size.height == 480 {
            cellWidth = frameHeight //is 3.5 inch device.
        } else {cellWidth = frameHeight/2}} //is regular iPhone or iPad.

        var spacing: CGFloat = 0
        if frameWidth > (4*cellWidth) {
            let remainingSpace = frameWidth - (6*cellWidth)
            spacing = remainingSpace/6 //For iPad
        } else {
            let remainingSpace = frameWidth - (3*cellWidth)
            spacing = remainingSpace/3 //For regular iPhone.
        }

        return spacing
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //Have selected sticker appear on photoTapViewController3
        let cell = collectionView.cellForItem(at: indexPath) as! photoTapCollectionCell
        let stickerView: UIImageView = cell.stickerView!
        let sticker = UIImage(named: (self.stickerNames?[indexPath.row])!)
       
        //This safely unwraps the value stored in the delegate property, meaning the delegate method is only invoked if the delegate property is set.
        if let delegate = self.delegate {
            delegate.stickerSelected(self, cellview: stickerView, sticker: sticker!)
        }
        
        //Since sticker has been added, make undo and save button selectable, and delete button unselectable:
        if let delegate2 = self.delegate2 {
            delegate2.makeDeleteSaveActive(true)
            delegate2.makeDeleteButtonActive(false)
        }
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        //#warning Incomplete method implementation -- Return the number of sections
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //#warning Incomplete method implementation -- Return the number of items in the section
        return self.stickerNames!.count
    }
    
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */
    
}
