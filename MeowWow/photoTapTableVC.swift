//
//  photoTapTableVC.swift
//  MeowWow
//
//  Created by Callum Morris on 10/04/15.
//  Copyright (c) 2015 Callum Morris. All rights reserved.
//

import UIKit

let cellIdentifyCategory = "cellIdentifyCategory"

class tableViewCellCategory : UITableViewCell{
    
    var textLabelCustom: UILabel = UILabel()
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.textLabelCustom.font = UIFont.systemFont(ofSize: 16)
        self.textLabelCustom.numberOfLines = 1 //When 0, there is no limit.
        self.textLabelCustom.lineBreakMode = NSLineBreakMode.byWordWrapping
        self.separatorInset = .zero
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.initViews()
    }
    
    func initViews() -> Void
    {   //Prep auto layout
        self.textLabelCustom.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(textLabelCustom)
        self.createConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//This protocol is to allow the container to be notified that a sticker category has been selected. The container is set as the delegate of the photoTapTableVC instance.
protocol categoryTappedDelegate: NSObjectProtocol {
    
    func controller(_ controller: photoTapTableVC, category: Int)
}

class photoTapTableVC: UITableViewController/*, UITableViewDelegate, UITableViewDataSource*/ {
    
    weak var delegate: categoryTappedDelegate?
    let categoryList: Array<String> = ["Hats","Hair","Glasses","Shoes","Food","Bling","Love","Accessories","Sport"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        //self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        // Setup
        self.tableView.register(tableViewCellCategory.self, forCellReuseIdentifier: cellIdentifyCategory)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.clearsSelectionOnViewWillAppear = true
        
        self.automaticallyAdjustsScrollViewInsets = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: tableViewCellCategory = tableView.dequeueReusableCell(withIdentifier: cellIdentifyCategory) as! tableViewCellCategory!
        let category: String = self.categoryList[(indexPath as NSIndexPath).row]
        cell.textLabelCustom.text = category
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return self.categoryList.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //This safely unwraps the value stored in the delegate property, meaning the delegate method is only invoked if the delegate property is set.
        if let delegate = self.delegate {
            delegate.controller(self, category: (indexPath as NSIndexPath).row)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    

